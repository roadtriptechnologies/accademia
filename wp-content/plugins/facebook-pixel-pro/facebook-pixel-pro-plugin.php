<?php
/*
Plugin Name: Facebook Pixel PRO
Description: Track everything
Version: 1.0
Author: Luca Micheli
Author URI: https://www.customerly.io/en/customer-support-live-chat-software?utm_medium=referral&utm_source=wordpress&utm_campaign=wordpressAuthorURI
*/


if ( ! defined('ABSPATH')){
    die();
}



add_action('wp_enqueue_scripts', 'fbpro_output_widget');


global $pagenow;


/*
 * Function that Render the actual widget in all the web pages
 */
function fbpro_output_widget()
{
    global $user_ID;

    
    if ('' == $user_ID) {
        //no user logged in
 
			   print("<!-- Facebook Pixel Code LUCAM-->
			<script>
			  !function(f,b,e,v,n,t,s)
			  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			  n.queue=[];t=b.createElement(e);t.async=!0;
			  t.src=v;s=b.getElementsByTagName(e)[0];
			  s.parentNode.insertBefore(t,s)}(window, document,'script',
			  'https://connect.facebook.net/en_US/fbevents.js');
			  fbq('init', '220965505676374');
			  fbq('track', 'PageView');
			</script>
			<noscript><img height=\"1\" width=\"1\" style=\"display:none\"
			  src=\"https://www.facebook.com/tr?id=220965505676374&ev=PageView&noscript=1\"
			/></noscript>
			<!-- End Facebook Pixel Code -->");
			
			
    } else {
	    
	    $current_user = wp_get_current_user();

	    $username = $current_user->user_login;
	    $email = $current_user->user_email;
	    $name = $current_user->display_name;
        //	$user = MeprUtils::get_currentuserinfo();
        print("<!-- Facebook Pixel Code LUCAM-->
			<script>
			  !function(f,b,e,v,n,t,s)
			  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			  n.queue=[];t=b.createElement(e);t.async=!0;
			  t.src=v;s=b.getElementsByTagName(e)[0];
			  s.parentNode.insertBefore(t,s)}(window, document,'script',
			  'https://connect.facebook.net/en_US/fbevents.js');
			  fbq('init', '220965505676374', {uid: '$user_ID'});
			  fbq('track', 'PageView');
			  
			  fbq('setUserProperties', '220965505676374', 
			    {
			      name: '$name', 
			      email: '$email',
			      username: '$username',
				  is_subscriber: ".(MeprUtils::is_subscriber() == 1 ? 1 : 0)."
			     });
				
			</script>
			<noscript><img height=\"1\" width=\"1\" style=\"display:none\"
			  src=\"https://www.facebook.com/tr?id=220965505676374&ev=PageView&noscript=1\"
			/></noscript>
			<!-- End Facebook Pixel Code -->");
			
		


				  
		}
				  
}


?>

��    5      �      l      l     m     r     w     {     �     �     �     �  4   �  /   �     �     
       	   $     .     F     U     c     g     l     {     �     �     �     �     �     �     �     �  
                  -     >     O     ]     f     �     �     �     �     �     �     �     �     �               "     2     7     ;  �  @     	     	      	     #	     '	     +	     /	     3	  3   7	  .   k	     �	     �	     �	  
   �	     �	     �	     �	     
     
     
      
     8
     ?
     P
     h
  "   u
     �
     �
     �
  
   �
     �
     �
     �
               -     5     U      \     }     �  	   �  
   �     �     �     �     �     �     �                   Fri  Mon  On  Sat  Sun  Thu  Tue  Wed %1$s scheduled a Zoom meeting %2$s in the group %3$s %1$s scheduled a Zoom meeting in the group %2$s %d hour %d hours %d minute %d minutes Copy Copy Link Copy Meeting Invitation Create Meeting Date and Time Day Days Delete Meeting Delete this Meeting Duration Edit Meeting Edit this Meeting Enable Zoom Enable join before host Enable waiting room Hours Meeting Details Meeting ID Meeting Link Meeting Options Meeting Passcode Meeting Password Meeting Title Meetings Meetings update complete! Minutes Mute participants upon entry New Zoom meeting No password required Participant Participants Past Meetings Second Seconds Upcoming Meetings Update Meeting View Invitation Zoom day days Project-Id-Version: BuddyBoss Platform Pro 1.0.4
Report-Msgid-Bugs-To: https://www.buddyboss.com/contact/
POT-Creation-Date: 2020-08-28 12:58:17+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2021-01-28 09:55+0000
Language-Team: Italiano
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Generator: Loco https://localise.biz/
Last-Translator: Marco
Language: it_IT
X-Poedit-SearchPath-0: .
X-Loco-Version: 2.5.0; wp-5.6 Ven Lun Il Sab Dom Gio Mar Mer %1$s ha programmato un webinar %2$s nel gruppo %3$s %1$s ha programmato un webinar nel gruppo %2$s %d ora %d ore %d minuto %d minuti Copia Copia Link Copia l'invito al webinar Crea Webinar Giorno e Ora Giorno Giorni Cancella Webinar Cancella questo webinar Durata Modifica Webinar Modifica questo Webinar Abilita Zoom Abilita l'ingresso prima dell'host Abilita la sala d'attesa Ore Informazioni Webinar Webinar ID Webinar link Opzioni del Webinar Codice segreto Webinar Password Webinar Titolo Webinar Webinar Aggiornamento Webinar completo! Minuti Silenzia WeShooters all'ingresso Nuovo Webinar Nessuna password richesta WeShooter WeShooters Webinar Passati Secondo Secondi Prossimi Webinar Aggiorna Webinar Visualizza invito Webinar giorno giorni 
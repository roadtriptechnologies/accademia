<?php
	
    
      if (!isset($_GET['txid'])){
	    die("Non dovresti accedere a questa pagina...");
    }
    
    $txid = $_GET['txid'];
    $trans_num = $_GET['trans_num'];
    $membership_id = $_GET['membership_id'];
    $amount = $_GET['amount'];
    $user_id = $_GET['user_id'];
    $redirect = $_GET['r'];
    
  
	
	?>
	
	
	<html>
		
		<head>
			
			<title>WeShoot Accademia</title>
			<meta property="og:title" content="WeShoot Accademia">
			<meta property="og:description" content="Accademia fotografia paesaggistica Italiana">
			<meta property="og:url" content="https://www.weshoot.it/accademia/">
			<meta property="og:image" content="https://www.weshoot.it/accademia/wp-content/uploads/2020/05/Dolomites-Adventures.jpg">
			<meta property="product:brand" content="WeShoot">
			<meta property="product:availability" content="in stock">
			<meta property="product:condition" content="new">
			<meta property="product:price:amount" content="9">
			<meta property="product:price:currency" content="EUR">
			<meta property="product:retailer_item_id" content="610">
			<meta property="product:item_group_id" content="9ubuzovepl">
			

			
			<script type="application/ld+json">
			{
			  "@context":"https://schema.org",
			  "@type":"Product",
			  "productID":"610",
			  "name":"WeShoot Accademia",
			  "description":"Accademia fotografia paesaggistica Italiana",
			  "url":"https://www.weshoot.it/accademia/",
			  "image":"https://www.weshoot.it/accademia/wp-content/uploads/2020/05/Dolomites-Adventures.jpg",
			  "brand":"facebook",
			  "offers": [
			    {
			      "@type": "Offer",
			      "price": "9",
			      "priceCurrency": "EUR",
			      "itemCondition": "https://schema.org/NewCondition",
			      "availability": "https://schema.org/InStock"
			    }
			  ],
			  "additionalProperty": [{
			    "@type": "PropertyValue",
			    "propertyID": "610",
			    "value": "9ubuzovepl"
			  }]
			}
			</script>


		<!-- Global site tag (gtag.js) - Google Ads: 618721279 -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=AW-618721279"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());
		
		  gtag('config', 'AW-618721279');
		</script>

		<!-- Event snippet for Purchase conversion page -->
		<script>
		gtag('event', 'conversion', {
		  'send_to': 'AW-618721279/qsJLCPbgxdYBEP_fg6cC',
		  'transaction_id': ''
		});
		</script>

			
			<!-- Facebook Pixel Code -->
		<script>
		  !function(f,b,e,v,n,t,s)
		  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		  n.queue=[];t=b.createElement(e);t.async=!0;
		  t.src=v;s=b.getElementsByTagName(e)[0];
		  s.parentNode.insertBefore(t,s)}(window, document,'script',
		  'https://connect.facebook.net/en_US/fbevents.js');
		  fbq('init', '220965505676374');
		  fbq('track', 'PageView');
		</script>
		
		<!-- End Facebook Pixel Code -->
			
		</head>
		
		<body>
			Attendi il reindirizzamento...
			<noscript>
		  <img height="1" width="1" style="display:none" 
		       src="https://www.facebook.com/tr?id=220965505676374&ev=PageView&noscript=1"/>
		</noscript>
		
		
		<script>
				
				document.addEventListener('DOMContentLoaded', (event) => {
					
					
					setTimeout(function(){
						
						// Verifico che esista LocalStorage
						if (localStorage){
							
							//Verifico che la transazione non esiste agia in LocalStorage
							if (localStorage.transactionID == undefined){
								console.log("Transazione non esistente, Traccio");
								localStorage.transactionID = '<?= $trans_num ?>';
								sendPurchase();
							}else{
								//Verifico che la transazione in localstorage non sia uguale a quella nuova
							  if ('<?= $trans_num ?>' != localStorage.transactionID){
								  console.log("Transazione differente da quella salvata, Traccio");
								  sendPurchase();
							  }else{
								  console.log("Transazione esistente non traccio");
								  redirect();
							  }
						  }
						  }else{
							  //Se non esiste localstorage faccio redirect diretto
							  console.log("Non esiste localStorage, rediretto");
							  redirect();
						  }
						  
					}, 300);
				
						
			});
			
			
				function sendPurchase(){
					fbq('track', 'Purchase', {
									    value: <?= $amount ?>,
									    currency: 'EUR',
									    content_ids: [610],
									    content_type: 'product',
									    contents: [
									      {
									        id: '610',
									        quantity: 1,
									        user_id: '<?= $user_id ?>',
									        membership_id: '<?= $membership_id ?>',
									        trans_num: '<?= $trans_num ?>',
									        
									      }],
									   
								
									  });
									  
					redirect();
				}
				
				function redirect(){
					
					console.log("Redirect");
					setTimeout(function(){
						console.log("Redirect a "+ '<?= $redirect ?>');
						window.location = '<?= $redirect ?>';
						}, 1500);
				}
				
				
				</script>
		
		</body>
	</html>
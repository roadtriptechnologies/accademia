=== GamiPress - Multimedia Content ===
Contributors: gamipress, tsunoa, rubengc, eneribs
Tags: media, audio, video, image, gamipress, gamification, points, achievements, badges, awards, rewards, credits, engagement, photo, multimedia, podcast, videocast
Requires at least: 4.4
Tested up to: 5.5
Stable tag: 1.0.3
License: GNU AGPLv3
License URI:  http://www.gnu.org/licenses/agpl-3.0.html

Add activity triggers based on multimedia content creation and interaction

== Description ==

GamiPress - Multimedia Content let's you add activity events based on multimedia content creation and interaction adding new activity events on [GamiPress](https://wordpress.org/plugins/gamipress/ "GamiPress")!

= New Events =

* Upload any multimedia content: When a user uploads any multimedia content.
* Upload an image: When a user uploads an image.
* Upload a video: When a user uploads a video.
* Upload an audio: When a user uploads an audio.
* Watch any video: When a user watches any video.
* Watch a specific video: When a user watches a specific video.
* Watch other user's video: When a user watches other user's video.
* Get a watch on a video: When a user gets a watch on a video.
* Listen any audio: When a user listen any audio.
* Listen a specific audio: When a user listen a specific audio.
* Listen other user's audio: When a user listen other user's audio.
* Get a listen on an audio: When a user gets a listen on an audio.

Important: The unique multimedia that triggers interaction activities are the media placed by WordPress [video] and [audio] shortcodes and WordPress Video and Audio widgets.

= Third Party Integrations =

GamiPress - Multimedia Content just let's you to add activity events based on self-hosted multimedia. 

There is a list of integrations with third party services we build:

* [Youtube](https://wordpress.org/plugins/gamipress-youtube-integration/)
* [Vimeo](https://wordpress.org/plugins/gamipress-vimeo-integration/)

== Installation ==

= From WordPress backend =

1. Navigate to Plugins -> Add new.
2. Click the button "Upload Plugin" next to "Add plugins" title.
3. Upload the downloaded zip file and activate it.

= Direct upload =

1. Upload the downloaded zip file into your `wp-content/plugins/` folder.
2. Unzip the uploaded zip file.
3. Navigate to Plugins menu on your WordPress admin area.
4. Activate this plugin.

== Frequently Asked Questions ==

== Screenshots ==

== Frequently Asked Questions ==

= Can this plugin work with YouTube or any third party videos supported by WordPress? =

Not yet, at this moment it just support video files you uploaded to WordPress.

= Can this plugin work with SoundCloud or any third party audios supported by WordPress? =

Not yet, at this moment it just support audio files you uploaded to WordPress.

== Changelog ==

= 1.0.3 =

* **Improvements**
* Improved gutenberg player detection.

= 1.0.2 =

* **New Features**
* Added events for watch

= 1.0.1 =

* **Bug Fixes**
* Fixed issue on activity triggers with default specific id.

= 1.0.0 =

* Initial release.

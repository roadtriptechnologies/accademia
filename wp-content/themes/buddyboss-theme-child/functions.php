<?php


$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once("$root/accademia/api/webhook.php");


/**
 * @package BuddyBoss Child
 * The parent theme functions are located at /buddyboss-theme/inc/theme/functions.php
 * Add your own functions at the bottom of this file.
 */


/****************************** THEME SETUP ******************************/

/**
 * Sets up theme for translation
 *
 * @since BuddyBoss Child 1.0.0
 */
function buddyboss_theme_child_languages()
{
    /**
     * Makes child theme available for translation.
     * Translations can be added into the /languages/ directory.
     */

    // Translate text from the PARENT theme.
    load_theme_textdomain('buddyboss-theme', get_stylesheet_directory() . '/languages');

    // Translate text from the CHILD theme only.
    // Change 'buddyboss-theme' instances in all child theme files to 'buddyboss-theme-child'.
    // load_theme_textdomain( 'buddyboss-theme-child', get_stylesheet_directory() . '/languages' );

}

add_action('after_setup_theme', 'buddyboss_theme_child_languages');

/**
 * Enqueues scripts and styles for child theme front-end.
 *
 * @since Boss Child Theme  1.0.0
 */
function buddyboss_theme_child_scripts_styles()
{
    /**
     * Scripts and Styles loaded by the parent theme can be unloaded if needed
     * using wp_deregister_script or wp_deregister_style.
     *
     * See the WordPress Codex for more information about those functions:
     * http://codex.wordpress.org/Function_Reference/wp_deregister_script
     * http://codex.wordpress.org/Function_Reference/wp_deregister_style
     **/

    // Styles
    wp_enqueue_style('buddyboss-child-css', get_stylesheet_directory_uri() . '/assets/css/custom.css', '', '1.0.0');

    // Javascript
    wp_enqueue_script('buddyboss-child-js', get_stylesheet_directory_uri() . '/assets/js/custom.js', '', '1.0.0');
}

add_action('wp_enqueue_scripts', 'buddyboss_theme_child_scripts_styles', 9999);


/****************************** CUSTOM FUNCTIONS ******************************/

// Add your own custom functions here

//Remove HeartBeat
add_action('init', 'stop_heartbeat', 1);

function stop_heartbeat()
{
    wp_deregister_script('heartbeat');
}

add_action('user_register', 'track_signup', 10, 1);

function track_signup($user_id)
{
    global $wpdb;

    $ip = gamipress_referrals_get_ip();

    $tab = "\t";
    $newLine = "\n";
    $message = "👨‍🎓 Nuovo WeShooter In Famiglia 🎉" . $newLine;
    $message .= "➡️ User ID: " . $user_id . $newLine;

    if (isset($_POST['googleLogin'])) {
        update_user_meta($user_id, 'googleLogin', $_POST['googleLogin']);
        $message .= "✅ Google Login" . $newLine;
        $user_info = get_userdata($user_id);
        $user_name = $user_info->display_name;
        $user_email = $user_info->user_email;
        $message .= "💌 Email: " . $user_email . $newLine;
        $message .= "➡️ Nome: " . $user_name . $newLine;

    } else {
        $message .= "❌ Normal Login" . $newLine;
        $message .= "💌 Email: " . $_POST['signup_email'] . $newLine;
        $message .= "➡️ Nome: " . $_POST['field_1'] . $newLine;
        $message .= "➡️ Username: " . $_POST['field_3'] . $newLine;
    }
    $message .= "👮 IP: " . $ip . $newLine . $newLine;


    if (isset($_POST['utm_source']) ||
        isset($_POST['utm_medium']) ||
        isset($_POST['utm_campaign']) ||
        isset($_POST['utm_content'])) {
        $message .= "🌶 UTMs:" . $newLine . $newLine . $newLine;
    }

    if (isset($_POST['utm_source'])) {
        update_user_meta($user_id, 'utm_source', $_POST['utm_source']);
        $message .= $tab . "➡️ Source: " . $_POST['utm_source'] . $newLine;

    }

    if (isset($_POST['utm_medium'])) {
        $message .= $tab . "➡️ Medium: " . $_POST['utm_medium'] . $newLine;
        update_user_meta($user_id, 'utm_medium', $_POST['utm_medium']);
    }


    if (isset($_POST['utm_campaign'])) {
        $message .= $tab . "➡️ Campaign: " . $_POST['utm_campaign'] . $newLine;

        update_user_meta($user_id, 'utm_campaign', $_POST['utm_campaign']);
    }


    if (isset($_POST['utm_content'])) {
        $message .= $tab . "➡️ Content: " . $_POST['utm_content'] . $newLine;
        update_user_meta($user_id, 'utm_content', $_POST['utm_content']);

    }

    if (isset($_POST['ref'])) {

        update_user_meta($user_id, 'ref', $_POST['ref']);


        $affiliate = gamipress_referrals_get_affiliate($_POST['ref']);


        // Return if affiliate not found
        if (!$affiliate) return;

        // Setup vars
        $affiliate_id = $affiliate->ID;


        $wpdb->get_results("SELECT * FROM wp_lm_referrals WHERE ip = '" . $ip . "'");
        $sameIPSignups = $wpdb->num_rows + 1;


        $wpdb->insert(
            'wp_lm_referrals',
            array(
                'user_id' => (int)$user_id,
                'referral_id' => $affiliate_id,
                'IP' => $ip,
                'date' => current_time('mysql', 1)
            )
        );

        $message .= $newLine . "🤙 Referral: " . $_POST['ref'] . $newLine;
        if ($sameIPSignups > 1) {
            $message .= "⚠️ Furbetto riconosciuto con $sameIPSignups registrazioni da questo IP" . $newLine;
            update_user_meta($affiliate_id, 'suspiciousReferrals', $sameIPSignups);
        } else {
            $message .= "✅ Prima registrazione da questo IP" . $newLine;
        }

    }

    if (isset($_POST['fbclid']))
        update_user_meta($user_id, 'fbclid', $_POST['fbclid']);


    Slack::send("controllo", $message);


}

function customerly_track_events()
{

    global $post;
    $post_slug = $post->post_name;
    $post_slug = str_replace("-", "_", $post_slug);

    if (is_user_logged_in()) {
        ?>
        <script type="text/javascript">
            setTimeout(
                function () {
                    customerly.event('<?php echo $post_slug; ?>');
                    window.dataLayer.push({'event': '<?php echo $post_slug; ?>'});
                }, 500
            )
        </script>
        <?php
    }

}

add_action('wp_head', 'customerly_track_events');

function initiate_GTM()
{

    ?>
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-5JSLPV3');</script>
    <!-- End Google Tag Manager -->
    <?php

    if (is_user_logged_in()) {
        $user_ID = get_current_user_id();
        ?>
        <script>
            dataLayer.push({"user_id": "<?php echo $user_ID; ?>"});
        </script>
        <?php
    }

}

add_action('wp_head', 'initiate_GTM');


add_action('init', 'set_utm_cookie');
function set_utm_cookie()
{

    global $utm;

    if (getenv('HTTP_CLIENT_IP'))
        $utm['ip'] = getenv('HTTP_CLIENT_IP');
    else if (getenv('HTTP_X_FORWARDED_FOR'))
        $utm['ip'] = getenv('HTTP_X_FORWARDED_FOR');
    else if (getenv('HTTP_X_FORWARDED'))
        $utm['ip'] = getenv('HTTP_X_FORWARDED');
    else if (getenv('HTTP_FORWARDED_FOR'))
        $utm['ip'] = getenv('HTTP_FORWARDED_FOR');
    else if (getenv('HTTP_FORWARDED'))
        $utm['ip'] = getenv('HTTP_FORWARDED');
    else if (getenv('REMOTE_ADDR'))
        $utm['ip'] = getenv('REMOTE_ADDR');
    else
        $utm['ip'] = 'UNKNOWN';

    $utm = array(
        'utm_source' => htmlspecialchars($_GET["utm_source"]),
        'utm_medium' => htmlspecialchars($_GET["utm_medium"]),
        'utm_term' => htmlspecialchars($_GET["utm_term"]),
        'utm_content' => htmlspecialchars($_GET["utm_content"]),
        'utm_campaign' => htmlspecialchars($_GET["utm_campaign"]),
        'ref' => htmlspecialchars($_GET["ref"]),
        'fbclid' => htmlspecialchars($_GET["fbclid"]),
        'ip' => $utm['ip']
    );


    if (!isset($_COOKIE['utm_source'])) {
        setcookie('utm_source', $utm['utm_source'], time() + 3600 * 60 * 24 * 7);
    }


    if (!isset($_COOKIE['utm_medium'])) {
        setcookie('utm_medium', $utm['utm_medium'], time() + 3600 * 60 * 24 * 7);
    }

    if (!isset($_COOKIE['utm_term'])) {
        setcookie('utm_term', $utm['utm_term'], time() + 3600 * 60 * 24 * 7);
    }

    if (!isset($_COOKIE['utm_content'])) {
        setcookie('utm_content', $utm['utm_content'], time() + 3600 * 60 * 24 * 7);
    }

    if (!isset($_COOKIE['utm_campaign'])) {
        setcookie('utm_campaign', $utm['utm_campaign'], time() + 3600 * 60 * 24 * 7);
    }

    if (!isset($_COOKIE['fbclid'])) {
        setcookie('fbclid', $utm['fbclid'], time() + 3600 * 60 * 24 * 7);
    }

    if (!isset($_COOKIE['ref'])) {
        setcookie('ref', $utm['ref'], time() + 3600 * 60 * 24 * 7);
    }


    if (!isset($_COOKIE['ip'])) {
        setcookie('ip', $utm['ip'], time() + 3600 * 60 * 24 * 7);
    }


}


add_action('woocommerce_payment_complete', 'payment_completed');


function payment_completed($order_id)
{
    $order = wc_get_order($order_id);
    $billingEmail = $order->billing_email;
    $products = $order->get_items();
    $tab = "\t";
    $newLine = "\n";
    $message = "💰 Nuovo Acquisto sullo store 🎉" . $newLine;
    $product_to_track = 10018; //12314   -> Webinar
    foreach ($products as $prod) {
        $items[$prod['product_id']] = $prod['name'];
        $message .= "Prodotto: " . $prod['name']. $newLine;
        if ($prod['product_id'] == $product_to_track) {
            $url = 'https://event.webinarjam.com/t/sale/1v2k4swcp?price=390.00';
            $response = wp_remote_get($url, array('timeout' => 8));
        }
        Slack::send("controllo", $message);
    }
}
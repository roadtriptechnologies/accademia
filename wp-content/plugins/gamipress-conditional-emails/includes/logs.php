<?php
/**
 * Logs
 *
 * @package     GamiPress\Conditional_Emails\Logs
 * @since       1.0.0
 */
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

/**
 * Register plugin log types
 *
 * @since 1.0.0
 *
 * @param array $gamipress_log_types
 *
 * @return array
 */
function gamipress_conditional_emails_logs_types( $gamipress_log_types ) {

    $gamipress_log_types['conditional_email_send'] = __( 'Conditional Email Send', 'gamipress-conditional-email' );

    return $gamipress_log_types;

}
add_filter( 'gamipress_logs_types', 'gamipress_conditional_emails_logs_types' );

/**
 * Log conditional email send on logs
 *
 * @since 1.0.0
 *
 * @param int|stdClass  $conditional_email_id
 * @param int           $user_id
 *
 * @return int|false
 */
function gamipress_conditional_emails_log_send( $conditional_email_id = null, $user_id = null ) {

    ct_setup_table( 'gamipress_conditional_emails' );

    $conditional_email = ct_get_object( $conditional_email_id );

    ct_reset_setup_table();

    // Can't register a not existent conditional email
    if( ! $conditional_email )
        return false;

    // Set the current user ID if not passed
    if( $user_id === null )
        $user_id = get_current_user_id();

    $subject = gamipress_conditional_emails_parse_subject( $user_id, $conditional_email );

    // Log meta data
    $log_meta = array(
        'pattern' => sprintf( __( 'Conditional email "%s" sent to {user}', 'gamipress-conditional-email' ), $subject ),
        'conditional_email_id' => $conditional_email_id,
    );

    // Register the conditional email send on logs
    return gamipress_insert_log( 'conditional_email_send', $user_id, 'private', '', $log_meta );

}

/**
 * Return the number of send an user receive from a specific conditional email
 *
 * @since 1.0.0
 *
 * @param int|stdClass  $conditional_email_id
 * @param int           $user_id
 *
 * @return int
 */
function gamipress_conditional_emails_get_user_sends( $conditional_email_id = null, $user_id = null ) {

    ct_setup_table( 'gamipress_conditional_emails' );

    $conditional_email = ct_get_object( $conditional_email_id );

    ct_reset_setup_table();

    // Can't register a not existent conditional email
    if( ! $conditional_email )
        return 0;

    // Set the current user ID if not passed
    if( $user_id === null )
        $user_id = get_current_user_id();

    return gamipress_get_user_log_count( $user_id, array(
        'type' => 'conditional_email_send',
        'conditional_email_id' => $conditional_email_id,
    ) );

}
<?php
/*
 *
 * valori nei campi del checkout
 *
 */

function billing_fields_woofc( $fields ) 
{


    global $woocommerce;
    $country = $woocommerce->customer->get_billing_country();

    if ($country !== 'IT') {
    
        $initaliasi = false;
    
    } else {

        $initaliasi = true;
        
    }


    $fields['billing_cod_fisc'] = array(
    'label'       => __('Fiscal Code', 'woo-fattureincloud'),
    'placeholder' => __('Here Fiscal Code', 'woo-fattureincloud'),
    'required'    => $initaliasi,
    'priority'    => 120,
    'class'       => array('form-row-wide'),
    );

    $fields['billing_partita_iva'] = array(
    'label'       => __('Vat Number', 'woo-fattureincloud'),
    'placeholder' => __('Vat Number Here', 'woo-fattureincloud'),
    'required'    => false,
    'priority'    => 130,
    'class'       => array('form-row-wide'),
    );

    $fields['billing_pec_email'] = array(
        'label'       => __('Email Pec', 'woo-fattureincloud'),
        'placeholder' => __('For Electronic Billing', 'woo-fattureincloud'),
        'required'    => false,
        'type'        => 'text',
        'priority'    => 140,
        'class'       => array('form-row-wide'),

    );

    $fields['billing_codice_destinatario'] = array(
    'label'       => __('Recipient Code', 'woo-fattureincloud'),
    'placeholder' => __('For Electronic Billing', 'woo-fattureincloud'),
    'required'    => false,
    'type'        => 'text',
    'priority'    => 150,
    'class'       => array('form-row-wide'),

    );

    return $fields;
}

/*
 *
 * valori modificabili dell'ordine
 *
 * */

function admin_billing_field( $fields ) 
{
    $fields['cod_fisc'] = array(
    'label' => __('Fiscal Code', 'woo-fattureincloud'),
    'wrapper_class' => 'form-field-wide',
    'show' => true,
    );

    $fields['partita_iva'] = array(
    'label' => __('Vat Number', 'woo-fattureincloud'),
    'wrapper_class' => 'form-field-wide',
    'show' => true,
    );

    $fields['pec_email'] = array(
    'label' => __('Email Pec', 'woo-fattureincloud'),
    'wrapper_class' => 'form-field-wide',
    'show' => true,
    );

    $fields['codice_destinatario'] = array(
    'label' => __('Recipient Code', 'woo-fattureincloud'),
    'wrapper_class' => 'form-field-wide',
    'show' => true,
    );

    return $fields;

}

####################################

function billing_ricevuta_wc_custom_checkout_field() 
{

        echo '<div id="billing_ricevuta_wc_custom_checkout_field">';
    
        woocommerce_form_field(
            'woorichiestaricevuta', array(
            'type'      => 'checkbox',
            'class'     => array('input-checkbox'),
            'label'     => __('tax receipt no invoice', 'woo-fattureincloud'),
            ), WC()->checkout->get_value('woorichiestaricevuta') 
        );
        echo '</div>';
    
}

function custom_checkout_field_update_order_meta( $order_id ) 
{ 
    if (! empty($_POST['woorichiestaricevuta']) ) { 
        update_post_meta($order_id, 'woorichiestaricevuta', sanitize_text_field($_POST['woorichiestaricevuta']));
    }
}


####################################
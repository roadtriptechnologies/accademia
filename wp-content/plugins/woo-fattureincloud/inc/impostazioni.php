<div id="woo_fattureincloud">
    <header></header>
</div>

	<h2>Inserire le API del sito fattureincloud.it: API >> MOSTRA API UID E API KEY </h2>
	<hr>
<?php

/**
 *
 * Form for API Value API UID and API KEY
 *
 */


?>

<table border="0" cellpadding="12" cellspacing="9">
    <tr>
            <td>

<form method="POST">

<?php wp_nonce_field(); ?>

            <label for="woo_fattureincloud_apiuid">API UID</label>

            <input type="password" name="api_uid_fattureincloud" placeholder="api uid"
                   value="<?php echo get_option('api_uid_fattureincloud'); ?>">

            <label for="woo_fattureincloud_apikey">API KEY</label>

            <input type="password" name="api_key_fattureincloud" placeholder="api key"
                   value="<?php echo get_option('api_key_fattureincloud'); ?>">

            </td>
 
        </tr>

   <!-- #################### -->
   <tr>
   <td style="background-color:#288CCC;color:white;">
<?php
        echo __('La finalizzazione della <b>Fattura Elettronica</b> si esegue su Fattureincloud.it ', 'woo-fattureincloud'); 
?>
<a style="color:white" href="http://bit.ly/2EEAOK4"> 📺 VIDEO</a>
        </td>

   </tr>


</table>



<table border="0" style="max-width:880px;" cellpadding="12" cellspacing="16" >


<!-- ################################################################# -->
<tr>

<td id="cella_numero_imp">1</td>

    <td bgcolor="white" width="33.3333%">
        
    <span class="dashicons dashicons-format-aside"></span> 

    <label for="fattureincloud_send_choice">
                <?php echo __('Abilita la creazione <b>manuale</b> di <br>', 'woo-fattureincloud');
                ?>
            </label>



    <input type="radio" id="fatturaelettronica_send_choice" name="fattureincloud_send_choice" value="fatturaelettronica"

        <?php
        if ('fatturaelettronica' == get_option('fattureincloud_send_choice')) {
            echo 'checked';
        } else {
            echo ''; 
        }
        ?> >

        <label for="contactChoice0">
            <?php echo __('FATTURA ELETTRONICA', 'woo-fattureincloud'); ?>
        </label>                

<br>

    <input type="radio" id="fattura_send_choice" name="fattureincloud_send_choice" value="fattura"

        <?php

        if ('fattura' == get_option('fattureincloud_send_choice')) {
            echo 'checked';
        } else {
            echo ''; 
        }
        ?> >
        
        <label for="contactChoice1">
            <?php echo __('FATTURA', 'woo-fattureincloud'); ?>
        </label>

<br>

    <input type="radio" id="ricevuta_disabled" name="fattureincloud_send_choice" value="ricevuta" disabled='disabled'>


        
        <label for="contactChoice1">
            <?php echo __('RICEVUTA*', 'woo-fattureincloud'); ?>
        </label>







    </td>

<!--################################################################################# -->

<td id="cella_numero_imp">2</td>

    <td bgcolor="white" width="33.3333%"> 

    <span class="dashicons dashicons-update"></span>
    <label for="fattureincloud_auto_save">
        <?php echo __('Quando l\'ordine è <b>Completato**</b> abilita la creazione in <b>Automatico</b> su Fattureincloud di <br>', 'woo-fattureincloud');
        ?>
    </label>

<br>

    <input type="radio" id="fatturaelettronica_auto_save" name="fattureincloud_auto_save" value="fatturaelettronica" disabled='disabled'>

    <label for="contactChoice0">
        <?php echo __('FATTURA ELETTRONICA ***', 'woo-fattureincloud'); ?>
    </label>
  
<br>    


    <input type="radio" id="ricevuta_fattureincloud_auto_save" name="fattureincloud_auto_save" value="ricevuta" disabled='disabled'>

    <label for="contactChoice2">
        <?php echo __('RICEVUTA ***', 'woo-fattureincloud'); ?>
    </label>
    
    
<br>
    
    <input type="radio" id="fattura_fattureincloud_auto_save" name="fattureincloud_auto_save" value="fattura"

        <?php

        if ('fattura' == get_option('fattureincloud_auto_save')) {
            echo 'checked';
        } else {
            echo ''; 
        }
        ?> >
    <label for="contactChoice1">
        <?php echo __('FATTURA', 'woo-fattureincloud'); ?>
    </label>


<br>


    <input type="radio" id="nulla" name="fattureincloud_auto_save" value= "nulla"

        <?php

        if ('nulla' == get_option('fattureincloud_auto_save')) {
            echo 'checked';
        } else {
            echo ''; 
        }

        ?>>

    <label for="contactChoice3">
        <?php echo __('NULLA', 'woo-fattureincloud'); ?>
    </label>

    </td>

<!-- ########################################################################################### -->

    <td id="cella_numero_imp">3</td>

    <td bgcolor="white" width="33.3333%"> 

    <span class="dashicons dashicons-cart"></span>
    <label for="fattureincloud_paid"><?php echo __( 'Enable the creation of a <b>paid invoice</ b>', 'woo-fattureincloud' );?></label>
        
        <input type="hidden" name="fattureincloud_paid" value="0" />
        <input type="checkbox" name="fattureincloud_paid" id="fattureincloud_paid" value="1" 
        <?php
        if (1 == get_option('fattureincloud_paid')) {
            echo 'checked';
        } else {
            echo '';
        }
        
        ?>>
    </td>

</tr>

<!-- ################################################################# -->

<tr>
<td id="cella_numero_imp">.</td>
<td style="background-color:#208CB3;color:white;a{color:#FFFFFF;}" width="33.3333%" align="center">
<?php echo __('*<b>RICEVUTA</b> solo nella', 'woo-fattureincloud'); ?>
<br><b><a style="color:white;" href="https://woofatture.com/shop/">Versione Premium</a></b>

</td>

<td id="cella_numero_imp">.</td>

<td style="background-color:#208CB3;color:white;link:white;" width="33.3333%">


<?php echo __('**oltre a <b>Completato</b> nella
<a style="color:white;" href="https://woofatture.com/shop/">
<b>Versione  Premium</b></a> è possibile impostare la creazione automatica 
quando l\'ordine si trova in modalità <br>
<b>IN LAVORAZIONE</b> oppure <br>
<b>IN SOSPESO</b>', 'woo-fattureincloud'); ?>

<?php echo __('<hr>***<b>FATTURA ELETTRONICA</b> e ***<b>RICEVUTA</b><br>automatica solo nella', 'woo-fattureincloud'); ?>
<br><b><a style="color:white;" href="https://woofatture.com/shop/">Versione Premium</a></b>

</td>

<td id="cella_numero_imp">4</td>

<td bgcolor="#e5e5e5" width="33.3333%" align="center">

<span class="dashicons dashicons-list-view"></span><br>

<label for="activate_customer_receipt">
            <?php echo __('Activate <br><b>tax receipt no invoice</b><br> choice in checkout', 'woo-fattureincloud');
            ?></label><br>

            <input type="hidden" name="activate_customer_receipt" value="0" />
            <input type="checkbox" name="activate_customer_receipt" id="activate_customer_receipt" value="1" 
            <?php
            if (1 == get_option('activate_customer_receipt') ) {
                echo 'checked';
            } else {
                echo '';
            }

            ?>>



</td>


    </tr>

<!-- ################################################################# -->


<tr>

<td id="cella_numero_imp">5</td>
<td bgcolor="#e5e5e5" width="33.3333%">

<span class="dashicons dashicons-id-alt"></span>

<label for="update_customer_registry">
            <?php echo __('Update the customer registry with the <b>personal data of the invoice</b>', 'woo-fattureincloud');
            ?></label>

            <input type="hidden" name="update_customer_registry" value="0" />
            <input type="checkbox" name="update_customer_registry" id="update_customer_registry" value="1" 
            <?php
            if (1 == get_option('update_customer_registry') ) {
                echo 'checked';
            } else {
                echo '';
            }

            ?>>
</td>

<td id="cella_numero_imp">6</td>

<td bgcolor="#e5e5e5" width="33.3333%">
<span class="dashicons dashicons-media-text"></span>
<label for="show_short_descr"><?php echo __('show in the invoice <b>short description</b> product', 'woo-fattureincloud');
            ?></label>

            <input type="hidden" name="show_short_descr" value="0" />
            <input type="checkbox" name="show_short_descr" id="show_short_descr" value="1"
            <?php
            if (1 == get_option('show_short_descr') ) {
                echo 'checked';
            } else {
                echo '';
            }

            ?>>
</td>

<td id="cella_numero_imp">7</td>

<td bgcolor="#e5e5e5" width="33.3333%">

<span class="dashicons dashicons-list-view"></span>

<label for="fattureincloud_partiva_codfisc">
        <?php echo __(
            'Attiva le voci nel checkout di <b>Partita Iva</b>, 
            <b>Codice Fiscale</b> (Solo se l\'indirizzo è italiano),<br> 
            <b>PEC</b> (per Fattura Elettronica), <b>Codice Destinatario</b> (per Fattura Elettronica)', 'woo-fattureincloud'
        );
        ?></label>

    <input type="hidden" name="fattureincloud_partiva_codfisc" value="0" />
    <input type="checkbox" name="fattureincloud_partiva_codfisc" id="fattureincloud_partiva_codfisc" value="1"
    <?php
    if (1 == get_option('fattureincloud_partiva_codfisc') ) {
        echo 'checked';
        
    } else {
        echo '';
    } 

    ?>>
</td>
</tr>


<!-- ################################################################# -->

<tr>
    <td align="right" colspan="6">
   
    <input type="submit" value="<?php echo __( 'Save Settings', 'woo-fattureincloud' );?>" class="button button-primary button-large"
    onclick="window.location='admin.php?page=woo-fattureincloud&tab=impostazioni#setting-error-settings_updated';">

</form>

</td>
</tr>

<!-- ################################################################# -->

<tr>
    <td colspan="6">

    Si consiglia di <b>verificare con molta attenzione</b> <br>che i dati per la Fattura Elettronica inviati a Fatureincloud siano corretti, <br>
        Gli autori del plugin declinano ogni responsabilità<br> per eventuali errori o mancanze nella generazione della Fattura Elettronica<br><br>

        I codici del tipo di pagamento (prelevati dallo SDI) con cui viene pre-compilata<br>
         la Fattura Elettronica sono :<br>
        <i>
         <ol>
            <li>    <b>MP08</b> carta di pagamento (carta di credito e PayPal)</li>
            <li>    <b>MP02</b> assegno bancario</li>
            <li>    <b>MP05</b> bonifico bancario</li>
            <li>    <b>MP01</b> contanti (pagamento in contrassegno)</li>
        </ol>    
        </i>
         se è stato utilizzato un <b>altro tipo di pagamento</b> è necessario modificarlo <b>direttamente su Fattureincloud.it</b> 


    </td>

</tr>




</table>



<a name="fic_premium"></a>
<p>Compra la <b><a href="https://woofatture.com/shop/">Versione Premium</a></b>! Oppure fai una <a href="#donate">Donazione</a></b></p>




<div id="promo_premium">
</div>




<table class="wp-block-table"bgcolor="#FFF" cellspacing="0" cellpadding="10">
<tbody>
    <tr>
        <td>&nbsp;</td>
        <td>Versione<br> Gratuita </td>
        <td><b> Versione<br> Premium</b></td>
        <td> <b>Versione Premium</b><br>
        Aliquote Iva 0%
        </td>
    </tr>
    <tr>
        <td style="text-align: center;background-color: #e3e3e3;">iva 22%</td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-yes-alt" style="color: green;"></span></td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-yes-alt" style="color: green; "></span></td>
        <td rowspan="27" style="padding:30px;vertical-align: top;" valign="top"><p>Nella versione <b>Premium</b> è possibile impostare<br>
        <b>44</b> tipologie specifiche di <b>Aliquota Iva = 0%</b><br>
        aggiungendola col rispettivo nome <b>Zero Rate</b> + <b>numero</b> in <br>
        <i>WooCommerce > Impostazioni > Imposta > Aliquote addizionali</i><br>
        maggiori informazioni nella <a href="https://woofatture.com/documentazione/#Aliquote_personalizzate_Iva_0_dalla_Versione_Premium_180">documentazione</a>
        </p>

<li><strong>Zero Rate 7</strong> = Regime dei minimi</li>
<li><strong>Zero Rate 49555</strong> = IVA Indetraibile</li>
<li><strong>Zero Rate 9</strong> = Fuori campo IVA</li>
<li><strong>Zero Rate 10</strong> = Oper. non soggetta, art.7 ter</li>
<li><strong>Zero Rate 11</strong> = Inversione contabile, art.7 ter</li>
<li><strong>Zero Rate 12</strong> = Non Imponibile</li>
<li><strong>Zero Rate 13</strong> = Non Imp. Art.8</li>
<li><strong>Zero Rate 14</strong> = Non Imp. Art.9 1C</li>
<li><strong>Zero Rate 15</strong> = Non Imp. Art.14 Legge 537/93</li>
<li><strong>Zero Rate 16</strong> = Non Imp. Art.41 D.P.R. 331/93</li>
<li><strong>Zero Rate 17</strong> = Non Imp. Art.72, D.P.R. 633/72</li>
<li><strong>Zero Rate 18</strong> = Non Imp. Art.74 quotidiani/libri</li>
<li><strong>Zero Rate 19</strong> = Escluso Art.10</li>
<li><strong>Zero Rate 20</strong> = Escluso Art.13 5C DPR 633/72</li>
<li><strong>Zero Rate 21</strong> = Escluso Art.15</li>
<li><strong>Zero Rate 22</strong> = Rev. charge art.17</li>
<li><strong>Zero Rate 23</strong> = Escluso Art.74 ter D.P.R. 633/72</li>
<li><strong>Zero Rate 24</strong> = Escluso Art.10 comma 1</li>
<li><strong>Zero Rate 25</strong> = Escluso Art.10 comma 20</li>
<li><strong>Zero Rate 26</strong> = Non Imp. Art.9</li>
<li><strong>Zero Rate 27</strong> = Escluso Art.10 n.27 D.P.R 633/72</li>
<li><strong>Zero Rate 30</strong> = Regime del margine art.36 41/95</li>
<li><strong>Zero Rate 31</strong> = Escluso Art.3 comma 4 D.P.R 633/72</li>
<li><strong>Zero Rate 32</strong> = Escluso Art.15/1c D.P.R 633/72</li>
<li><strong>Zero Rate 33</strong> = Non imp. Art.8/c D.P.R. 633/72</li>
<li><strong>Zero Rate 34</strong> = Non Imp. Art.7 ter”</li>
<li><strong>Zero Rate 35</strong> = Escluso Art.7 D.P.R 633/72</li>
<li><strong>Zero Rate 37</strong> = Escluso Art.10 comma 9</li>
<li><strong>Zero Rate 38</strong> = Non imp. Art.7 quater DPR 633/72</li>
<li><strong>Zero Rate 39</strong> = Non Imp. Art.8 comma 1A</li>
<li><strong>Zero Rate 42</strong> = Non Imp. Art.2 comma 4 D.P.R 633/72</li>
<li><strong>Zero Rate 43</strong> = Non Imp. Art.18 633/72</li>
<li><strong>Zero Rate 44</strong> = Fuori Campo IVA Art.7 ter D.P.R 633/72</li>
<li><strong>Zero Rate 45</strong> = Non Imp. Art.10 n.18 DPR 633/72</li>
<li><strong>Zero Rate 46</strong> = Esente Art.10 DPR 633/72</li>
<li><strong>Zero Rate 47</strong> = Non imp. art.1 L. 244/2008</li>
<li><strong>Zero Rate 48</strong> = Non imp. art.40 D.L. 427/93</li>
<li><strong>Zero Rate 49</strong> = Non imp. art.41 D.L. 427/93</li>
<li><strong>Zero Rate 50</strong> = Non imp. art.71 DPR 633/72</li>
<li><strong>Zero Rate 51</strong> = Non imp. art.8 DPR 633/72</li>
<li><strong>Zero Rate 52</strong> = Non imp. art.9 DPR 633/72</li>
<li><strong>Zero Rate 53</strong> = Regime minimi 2015</li>
<li><strong>Zero Rate 66</strong> = Contribuenti forfettari</li>
<li><strong>Zero Rate 236663</strong> = Rev. charge art.17</li>


(i salti nella numerazione non sono un errore) 
        
        
        </td>
    </tr>
    <tr>
        <td style="text-align: center"> iva 0%</td>
        <td style="text-align: center"><span class="dashicons dashicons-yes-alt" style="color: green; "></span></td>
        <td style="text-align: center"><span class="dashicons dashicons-yes-alt" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center;background-color: #e3e3e3;">iva 24%</td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-dismiss" style="color: red; "></span></td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-yes-alt" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center">iva 23%</td>
        <td style="text-align: center"><span class="dashicons dashicons-dismiss" style="color: red; "></span></td>
        <td style="text-align: center"><span class="dashicons dashicons-yes-alt" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center;background-color: #e3e3e3;">iva 21%</td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-dismiss" style="color: red; "></span></td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-yes-alt" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center">iva 20%</td>
        <td style="text-align: center"><span class="dashicons dashicons-dismiss" style="color: red; "></span></td>
        <td style="text-align: center"><span class="dashicons dashicons-yes-alt" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center;background-color: #e3e3e3;">iva 10%</td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-dismiss" style="color: red; "></span></td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-yes-alt" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center">iva 4%</td>
        <td style="text-align: center"><span class="dashicons dashicons-dismiss aligncenter" style="color: red; "></span></td>
        <td style="text-align: center"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center;background-color: #e3e3e3;">iva 5%</td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-dismiss aligncenter" style="color: red; "></span></td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center">iva 8%</td>
        <td style="text-align: center"><span class="dashicons dashicons-dismiss aligncenter" style="color: red; "></span></td>
        <td style="text-align: center"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center;background-color: #e3e3e3;">Selezione ultimi 10 ordini</td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center">Ricerca ordine</td>
        <td style="text-align: center"><span class="dashicons dashicons-dismiss aligncenter" style="color: red; "></span></td>
        <td style="text-align: center"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center;background-color: #e3e3e3;">Crea Fattura</td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center">Crea Fattura Elettronica</td>
        <td style="text-align: center"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
        <td style="text-align: center"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center;background-color: #e3e3e3;">Crea Ricevuta</td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-dismiss aligncenter" style="color: red; "></span></td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center">Invia Fattura via email</td>
        <td style="text-align: center"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
        <td style="text-align: center"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center;background-color: #e3e3e3;">Creazione Automatica Fattura</td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center">Creazione Automatica<br>Fattura Elettronica </td>
        <td style="text-align: center"><span class="dashicons dashicons-dismiss aligncenter" style="color: red; "></span></td>
        <td style="text-align: center"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center;background-color: #e3e3e3;">Creazione Automatica<br>Ricevuta</td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-dismiss aligncenter" style="color: red; "></span></td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center">Invia Ricevuta via email</td>
        <td style="text-align: center"><span class="dashicons dashicons-dismiss aligncenter" style="color: red; "></span></td>
        <td style="text-align: center"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center;background-color: #e3e3e3;">Invia Fattura Elettronica<br>(copia cortesia) via email</td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-dismiss aligncenter" style="color: red; "></span></td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center">Aggiungere voce CF e pIva</td>
        <td style="text-align: center"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
        <td style="text-align: center"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center;background-color: #e3e3e3;">Invio Email Automatico</td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-dismiss aligncenter" style="color: red; "></span></td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center">Iva 0% personalizzabile</td>
        <td style="text-align: center"><span class="dashicons dashicons-dismiss aligncenter" style="color: red; "></span></td>
        <td style="text-align: center"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center;background-color: #e3e3e3;">Sezionali</td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-dismiss aligncenter" style="color: red; "></span></td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center">Codice Fiscale obbligatorio<br> opzionale</td>
        <td style="text-align: center"><span class="dashicons dashicons-dismiss aligncenter" style="color: red; "></span></td>
        <td style="text-align: center"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center;background-color: #e3e3e3;">Descrizione completa prodotto</td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-dismiss aligncenter" style="color: red; "></span></td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center">Disabilita creazione documento<br>se importo ordine uguale a zero</td>
        <td style="text-align: center"><span class="dashicons dashicons-dismiss aligncenter" style="color: red; "></span></td>
        <td style="text-align: center"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
    <tr>
        <td style="text-align: center;background-color: #e3e3e3;">Abilita differenti aliquote<br> nei prodotti variabili</td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-dismiss aligncenter" style="color: red; "></span></td>
        <td style="text-align: center;background-color: #e3e3e3;"><span class="dashicons dashicons-yes-alt aligncenter" style="color: green; "></span></td>
    </tr>
</tbody>
</table>



<hr>
<a name="donate"></a>
<p>Trovi utile questo plugin <b>WooCommerce Fattureincloud</b> per la tua attività?<br>
Apprezzi il fatto che sia gratuito e vuoi fare liberamente una donazione?<br>
Clicca su <b>"Donate"</b>!<br>
Anche un piccolo gesto è importante per poter continuare ad offrire questo servizio <b>gratuitamente</b><br>
Grazie!<br>
PS: la Donazione NON da il diritto ad utilizzare la versione Premium del plugin <b>WooCommerce Fattureincloud</b>
</p>

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick" />
<input type="hidden" name="hosted_button_id" value="U7VN7ATH4GEDY" />
<input type="image" src="https://www.paypalobjects.com/en_US/IT/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
<img alt="" border="0" src="https://www.paypal.com/en_IT/i/scr/pixel.gif" width="1" height="1" />
</form>


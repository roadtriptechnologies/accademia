<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'moosend/mootracker',
  ),
  'versions' => 
  array (
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '6.3.3',
      'version' => '6.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '407b0cb880ace85c9b63c5f9551db498cb2d50ba',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => 'v1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a59da6cf61d80060647ff4d3eb2c03a2bc694646',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.4.2',
      'version' => '1.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f5b8a8512e2b58b0071a7280e39f14f72e05d87c',
    ),
    'moosend/mootracker' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'moosend/website-tracking' => 
    array (
      'pretty_version' => '1.12.55',
      'version' => '1.12.55.0',
      'aliases' => 
      array (
      ),
      'reference' => '6ea91d6c6ded72fda1b3e9a31e2fb9d5edac432e',
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
  ),
);

<?php
	
class Response
{

    static function JSON($array, $userData = false, $utf8Encode = false, $plainData = false)
    {
        header('Content-Type: application/json');

        if (!$plainData) {
            $toPrint = array(
                'data' => $array,
                'timestamp' => time()
            );
        } else {
            $toPrint = $array;
        }


        if ($userData) {
            $toPrint = array_merge($toPrint, Authorization::userData());
        }



        if ($utf8Encode) {
            array_walk_recursive(
                $toPrint, function (&$value) {
                if (is_string($value)) {
                    $value = utf8_encode($value);
                } else {
                    $value = $value;
                }

            }
            );
        }

        print_r(json_encode($toPrint, JSON_UNESCAPED_UNICODE));


    }


}


	/**
	* Admin
	*/
	class Admin  {
	
						
		static function isAuthorized(){
			

			//user => password
			$users = array('roadtrip' => 'FaremoTantiEuri');
		
			
			if (isset($_SERVER['PHP_AUTH_USER'])) {
			    $username = $_SERVER['PHP_AUTH_USER'];
			    $password = $_SERVER['PHP_AUTH_PW'];
			    			    
			    if (!isset($users[$username])){
						http_response_code(401);	
						die();		    
				}else{
				    if ($users[$username] == $password){
					   	http_response_code(200);
					   	return;
				    }
			    }
			    
			 }else{
				 http_response_code(401);	
				 die();	
			 }
				
					    
    
		}
		
		static function getChargifyData(){
			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://customerly-limited.chargify.com/stats.json",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_POSTFIELDS => "{}",
			  CURLOPT_HTTPHEADER => array(
			    "authorization: Basic NnJ1bnBWREpsSnhmMVRscTJRR0JUOUJCRTJmWGVKb0dHdU8za3BodEFFOg==",
			    "content-type: application/json"
			  ),
			));
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			
			curl_close($curl);

			if ($err) {
			  echo "cURL Error #:" . $err;
			} else {
				return json_decode($response);			
			}
		}
		
		
		static function total_subscriptions(){
			
			$stats = Admin::getChargifyData();
			return array("postfix" => "Units",  "data" => array( "value" => $stats->stats->total_active_subscriptions) );
			
		}
		static function cleanValue($value){
			$value = str_replace("$", "", $value);
			$value = str_replace(",", "", $value);
			return floatval($value);

		}
		
		static function total_revenue(){
			$stats = Admin::getChargifyData();
			return array("postfix" => "$",  "data" => array( "value" => Admin::cleanValue($stats->stats->total_revenue)) );
			
		}
		
		static function revenue_this_month(){
			$stats = Admin::getChargifyData();
			return array("postfix" => "$",  "data" => array( "value" => Admin::cleanValue($stats->stats->revenue_this_month)) );
			
		}
		
		static function revenue_today(){
			$stats = Admin::getChargifyData();
			return array("postfix" => "$",  "data" => array( "value" => Admin::cleanValue($stats->stats->revenue_today)) );
		}
		
		
		static function total_dunning_subscriptions(){
			$stats = Admin::getChargifyData();
			return array("postfix" => "Dunning",  "data" => array( "value" => Admin::cleanValue($stats->stats->total_dunning_subscriptions)));
			
		}
		
		static function subscriptions_today(){
			$stats = Admin::getChargifyData();
			return array("postfix" => "Subscriptions",  "data" => array( "value" => Admin::cleanValue($stats->stats->subscriptions_today)) );
			
		}
		
		static function total_past_due_subscriptions(){
			$stats = Admin::getChargifyData();
			return array("postfix" => "Past Due",  "data" => array( "value" => Admin::cleanValue($stats->stats->total_past_due_subscriptions)) );
		}
				
				
				
	
	}
	
	
	switch ($_SERVER['REQUEST_METHOD']){
		

		case "GET":
			
			if (!isset($_GET["f"])) return;
			Admin::isAuthorized();
			
			switch($_GET["f"]){
				case "total_subscriptions":
					Response::JSON(Admin::total_subscriptions(), false, false, true);					
				break;
							
				case "total_revenue":
					Response::JSON(Admin::total_revenue(), false, false, true);					
				break;	 
				
				case "revenue_this_month":
					Response::JSON(Admin::revenue_this_month(), false, false, true);					
				break;
				
				case "revenue_today":
					Response::JSON(Admin::revenue_today(), false, false, true);					
				break;
				 
				case "total_dunning_subscriptions":
					Response::JSON(Admin::total_dunning_subscriptions(), false, false, true);					
				break;	 
				
				case "subscriptions_today":
					Response::JSON(Admin::subscriptions_today(), false, false, true);					
				break;
				
				case "total_past_due_subscriptions":
					Response::JSON(Admin::total_past_due_subscriptions(), false, false, true);					
				break;
				 
		
				 
				
			}


		break;

	}

	
	
	
	

	
?>
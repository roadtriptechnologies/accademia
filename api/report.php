<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once("$root/accademia/api/webhook.php");


class Report
{

    static function referralSignup($affiliate_id, $referral_id, $referral_ip, $affiliate, $affiliate_data, $referral)
    {

        $newLine = "\n";
        $message = "🎰 New Signup from Affiliate" . $newLine;

        $message .= "🗣 Affiliate: " . $affiliate_data->nickname . $newLine;
        $message .= "🗣 ‍‍ID: " . $affiliate_id . $newLine;
        $message .= "🗣 Email: " . $affiliate_data->user_email . $newLine;
        $message .= "🗣 Coins: " . $affiliate_data->_gamipress_coins_points . $newLine . $newLine;


        $message .= "➡️ Referral: " . $referral->user_login . $newLine;
        $message .= "➡️ ID: " . $referral_id . $newLine;
        $message .= "➡️ Email: " . $referral->user_email . $newLine;
        $message .= "➡️ IP: " . $referral_ip . $newLine;


        Slack::send("controllo", $message);
    }


}
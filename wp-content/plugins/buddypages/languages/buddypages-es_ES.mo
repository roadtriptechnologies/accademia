��    N      �  k   �      �     �     �     �  
   �  	   �     �  	   �     �  �     	   �     �  
   �  �   �  -   H     v     �     �     �     �  	   �  	   �     �     �     �     	     	  ;   &	  �   b	     �	     �	  
   
     
     (
  *   5
  
   `
     k
  	   t
     ~
     �
     �
     �
     �
     �
     �
  q   �
     \     d  !   p  !   �     �     �     �     �     �     �               0     N     g  i   x     �     �     �     
  	      ,   *  2   W  "   �  '   �  "   �  (   �     !     <     \     c     x  �  �           .     E     X     i     }     �  %   �  �   �  	   r     |  
   �  �   �  9   )     c  "   v  	   �     �     �     �     �     �     �          '     6  D   Q  �   �     0     E     [  "   n     �  .   �  	   �     �     �     �          *     <     E     U     l  �   ~            
   4  	   ?     I     W     ^     g          �     �  )   �  *   �  %        :  o   P     �     �     �     �       '     0   @     q  !   �  $   �  0   �     	     #     B     I     ^     #       4   "          K   2   A                  
   B      $       J   %      -                         /   =   @         '   	       )                            G          >              5   &   !       1                 8   <                    ,   ;   E   I   *          F      .       6   3   7          M       9   C          0   H   L   N   ?          :   (                  D       +    Add New Add New Item Add Page All Groups All Items All Members All Users Allow Members to Create Pages Allow site members to create pages for profiles and groups. Site administrators can always create BuddyPages, regardless of this setting. BuddyPage BuddyPage Settings BuddyPages BuddyPages is missing requirements and has been <a href="%s">deactivated</a>. Please make sure BuddyPress is installed and activated. BuddyPress user pages for profiles and groups Changes saved. Check out %s from @pluginize Content Draft Edit Edit Item Edit Page Error adding page. Error deleting page. Error editing page. Featured Image Filter items list Front end page creation for BuddyPress profiles and groups. Helpscout Scanner is missing requirements and has been <a href="%s">deactivated</a>. Please make sure all requirements are available. Insert into item Item Archives Items list Items list navigation Member Pages More plugins for your WordPress site here! My Profile New Item Not found Not found in Trash Page added. Page deleted. Pages Parent Item: Permanent Link to edit %s Pluginize Support Pluginize products expand your ability to win the race with plugins used by companies looking for proven results. Post In Post Status Post Type General NameBuddyPages Post Type Singular NameBuddyPage Posted in:  Profile Publish Remove featured image Search Help Docs Search Item Set featured image Sorry, currently no news. Sorry, currently no products. Sorry, no pages created. Spread the word! Thank you for choosing Pluginize! Here you can see our latest posts, search for help, or submit a ticket. Title Update Item Uploaded to this item Use as featured image View Item Your server does not have SUHOSIN installed. Your server does not have the SOAP Client enabled. Your server does not support cURL. Your server does not support fsockopen. Your server has SUHOSIN installed. Your server has the SOAP Client enabled. Your server supports cURL. Your server supports fsockopen. delete http://pluginize.com post status is draft Project-Id-Version: BuddyPages
PO-Revision-Date: 2021-03-30 20:48-0500
Last-Translator: Michael Beckwith <michael.d.beckwith@gmail.com>
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
X-Poedit-Basepath: ..
X-Poedit-WPHeader: loader.php
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Añadir nuevo Agregar nuevo elemento ﻿Agregar Página Todos los Grupos Todos los Elementos Todos los miembros Todos los Usuarios Permite a los miembros crear páginas Permitir a los miembros del sitio crear páginas de perfiles y grupos. Los administradores del sitio pueden crear siempre BuddyPages, independientemente de este ajuste. BuddyPage Ajustes BuddyPage BuddyPages A BuddyPages le faltan requisitos y han sido <a href="%s">desactivados</a> . Por favor asegúrese de que BuddyPress está instalado y activado. BuddyPress páginas de usuario para los perfiles y grupos Cambios guardados. Echa un vistazo a %s de @pluginize Contenido Borrador Editar Editar Elemento Editar Página Error al añadir la página. Error al eliminar la página Error al editar la página Foto principal Filtrar Lista de Elementos Creación de páginas Front End para perfiles y grupos de Buddypress A Helpscout escáner le faltan requisitos y han sido <a href="%s">desactivados</a> . Por favor asegúrese de que todos los requisitos están disponibles. Insertar en elemento Archivos de elementos Lista de elementos Lista de posiciones de navegación Páginas de miembro Más plugins para su sitio de WordPress aquí! Mi Perfil Nuevo elemento No encontrado No encontrado en la papelera Página añadida. Página eliminada Páginas Elemento padre: Enlace permanente a %s Soporte Pluginize Los productos Pluginize expanden su capacidad para ganar la carrera con los plugins utilizados por las empresas que buscan resultados probados. Publicar en Estado de la publicación BuddyPages BuddyPage Publicado en: Perfil Publicar Eliminar foto principal Búsque Docs de ayuda Buscar Elemento Establecer foto principal Lo sentimos, actualmente no hay noticias. Lo sentimos, actualmente no hay productos. Lo sentimos, no hay páginas creadas. ¡Difunde la palabra! Gracias por elegir Pluginize! Aquí se pueden ver nuestros últimos mensajes, buscar ayuda, o enviar un ticket. Titulo Actualizar Elemento Subido a este elemento Usar como foto principal Ver Elemento Su servidor no tiene instalado Suhosin. Su servidor no tiene habilitado el cliente SOAP. Su servidor no soporta el cURL. Su servidor no soporta fsockopen. El servidor tiene instalado Suhosin. El servidor tiene el cliente de SOAP habilitado. Su servidor soporta cURL. Su servidor soporta fsockopen. Borrar http://pluginize.com estado del post en borrador 
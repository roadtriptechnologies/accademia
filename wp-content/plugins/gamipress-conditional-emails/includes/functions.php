<?php
/**
 * Functions
 *
 * @package     GamiPress\Conditional_Emails\Functions
 * @since       1.0.0
 */
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

/**
 * Send conditional email to user given
 *
 * @since 1.0.0
 *
 * @param int           $user_id
 * @param int|stdclass  $conditional_email
 *
 * @return bool
 */
function gamipress_conditional_emails_send_email( $user_id, $conditional_email ) {

    $is_test = ( defined( 'GAMIPRESS_CONDITIONAL_EMAILS_TEST_SEND' ) && GAMIPRESS_CONDITIONAL_EMAILS_TEST_SEND );

    $prefix = '_gamipress_conditional_emails_';

    // Setup table
    ct_setup_table( 'gamipress_conditional_emails' );
    $conditional_email = ct_get_object( $conditional_email );

    // Bail if conditional email not exists
    if( ! $conditional_email ) return false;

    // Shorthand
    $id = $conditional_email->conditional_email_id;

    $send_email = true;

    // Bail if conditional email is not active
    if( $send_email && $conditional_email->status !== 'active' )
        $send_email = false;

    $user = get_userdata( $user_id );

    // Bail if user not exists
    if( $send_email && ! $user )
        $send_email = false;

    // Don't check max sends on test email
    if( ! $is_test ) {

        // Check max sends
        $sends = absint( ct_get_object_meta( $id, $prefix . 'sends', true ) );
        $max_sends = absint( ct_get_object_meta( $id, $prefix . 'max_sends', true ) );

        // Check if max sends has been exceeded
        if( $max_sends !== 0 && $sends >= $max_sends )
            $send_email = false;

        // Check max sends per user
        $user_sends = gamipress_conditional_emails_get_user_sends( $id, $user_id );
        $max_sends_per_user = absint( ct_get_object_meta( $id, $prefix . 'max_sends_per_user', true ) );

        // Max user uses
        if( $max_sends_per_user !== 0 && $user_sends >= $max_sends_per_user )
            $send_email = false;

    }

    /**
     * Filter to determine if a conditional email should be sent
     *
     * @since 1.0.0
     *
     * @param bool      $send_email
     * @param int       $user_id
     * @param int       $conditional_email_id
     * @param stdClass  $conditional_email
     *
     * @return bool
     */
    if( ! apply_filters( 'gamipress_conditional_emails_send_email', $send_email, $user_id, $id, $conditional_email ) ) {
        return false;
    }

    // Parse subject and content
    $subject = gamipress_conditional_emails_parse_subject( $user_id, $conditional_email );
    $content = gamipress_conditional_emails_parse_content( $user_id, $conditional_email );

    // Don't increase sends and don't log send if is a test send
    if( ! $is_test ) {
        // Increase email sends
        ct_update_object_meta( $id, $prefix . 'sends', ( $sends + 1 ), $sends );

        // Log conditional email send
        gamipress_conditional_emails_log_send( $id, $user_id );
    }

    ct_reset_setup_table();

    // Send email to the user
    return gamipress_send_email( $user->user_email, $subject, $content );

}

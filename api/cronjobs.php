<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once("$root/accademia/api/stats.php");

class Cronjob
{
    static function execute($function)
    {
        switch ($function) {
            case "syncUserInfo":
                Stats::syncUserInfo();
                break;

            case "getUserInfo":
                Stats::getUserInfo();
                break;

            case "getWeeklySignups":
                Stats::getWeeklySignups();
                break;

            case "getTodaySignups":
                Stats::getTodaySignups();
                break;
        }
    }
}


if (isset($_GET['f'])) {
    Cronjob::execute($_GET['f']);
}
=== GamiPress - Conditional Emails ===
Contributors: gamipress, tsunoa, rubengc, eneribs
Tags: gamipress, gamification, point, achievement, rank, badge, award, reward, credit, engagement, ajax
Requires at least: 4.4
Tested up to: 5.6
Stable tag: 1.0.8
License: GNU AGPLv3
License URI: http://www.gnu.org/licenses/agpl-3.0.html

Automatically send emails based on pre-defined conditions.

== Description ==

Conditional Emails let's you setup automatic emails when certain events occur to bring to your users a specific email and build engagement with them on exact moment you want.

In just a few minutes, you will be able to build a personalized email to being sent on relevant events like reach a points balance, unlock an achievement or reach a rank among others.

In addition, Conditional Emails includes a huge list of tags to let you display relevant information to your users like their current points balance, last achievements earned and current rank of any type.

= Features =

* Send customizable emails when a certain event occur.
* Set the condition you want: reach a points balance, unlock an achievement, unlock any achievement of type, unlock all achievements of type or reach a rank.
* Tags per type to inform users about their current points balance, last achievements earned and current rank of any type.
* Ability to send a test email to yourself.
* Ability to limit the total number of sends of a conditional email.
* Ability to limit the maximum number of sends of a conditional email per user.

== Installation ==

= From WordPress backend =

1. Navigate to Plugins -> Add new.
2. Click the button "Upload Plugin" next to "Add plugins" title.
3. Upload the downloaded zip file and activate it.

= Direct upload =

1. Upload the downloaded zip file into your `wp-content/plugins/` folder.
2. Unzip the uploaded zip file.
3. Navigate to Plugins menu on your WordPress admin area.
4. Activate this plugin.

== Frequently Asked Questions ==

== Changelog ==

= 1.0.8 =

* **Improvements**
* Improved styles at admin area.
* Updated GamiPress functions.

= 1.0.7 =

* **Improvements**
* Improved compatibility with multisite installs.

= 1.0.6 =

* **Bug Fixes**
* Fixed achievements and ranks selector.
* **Improvements**
* Style improvements on the admin area.

= 1.0.5 =

* **New Features**
* Added support to GamiPress 1.8.0.

= 1.0.4 =

* **Bug Fixes**
* Fixed wrong email sending when user earns points of a different type.

= 1.0.3 =

* **New Features**
* Added the ability to limit the maximum number of emails to send.
* Added the ability to limit the maximum number of emails to send per user.

= 1.0.2 =

* **Improvements**
* Improvements on functions to parse tags on email subject and content.
* Performance improvements to avoid repeatedly retrieve the same conditional email information from database.
* **Developer Notes**
* Added new hooks to make plugin more flexible and extensible.

= 1.0.1 =

* **New Features**
* Added support to GamiPress 1.7.0.

= 1.0.0 =

* Initial Release.

<?php
$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once("$root/accademia/api/conf.php");

class Customerly
{
    static function createUsers($users)
    {
        $ch = curl_init();

        $payload = array(
            'users' => $users
        );

        curl_setopt($ch, CURLOPT_URL, "https://api.customerly.io/v1/users");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Authentication: AccessToken: " . CLY_API
        ));

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
}
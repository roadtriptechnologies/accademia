<?php

// Don't access this directly, please
if (!defined('ABSPATH')) exit;

?>

<table border="0" cellpadding="6">
    <tr>
            <td align="right">


            <form id="woo-fattureincloud-anno-fatture" method="POST">

<?php wp_nonce_field(); ?>

            <label for="woo_fattureincloud_apiuid">Anno Fatture</label>

            <input type="number" name="woo-fattureincloud-anno-fatture" placeholder="anno"
                   value="<?php echo get_option('woo-fattureincloud-anno-fatture'); ?>">

            </td>
            <td>

                    <input type="submit" value="Save" class="button button-primary button-large">
            </td>
            </form>

        </tr>
</table>
    <div id="fatture-elenco">


<?php


$api_uid = get_option('api_uid_fattureincloud');
$api_key = get_option('api_key_fattureincloud');
$annofatture = get_option('woo-fattureincloud-anno-fatture');

$url = "https://api.fattureincloud.it:443/v1/fatture/lista";


include plugin_dir_path(__FILE__) . 'conn_curl.php';

//if(!$result){die("Connection Failure");}

/*
echo "<pre>";
print_r($result);
echo "</pre>";
echo "<hr>";
*/

if (is_array($result)) {

    if (! in_array("success", $result)) {

        ?>
        <div id="message" class="notice notice-error is-dismissible">
        <p><b>Elenco Fatture non Scaricato:

        <?php
        echo $result['error'];
        ?>
        </b>
        </div>
        <?php
    }

    if (!isset($result['numero_risultati'])) {

        echo "Impossibile scaricare elenco Fatture su Fattureincloud.it";

    }


    foreach ($result as $value) {

        if (is_array($value)) {

            $count = 0;

            foreach ($value as $value2) {

                $count = $count + 1;

                /*echo $value2[$i];*/

                //echo "<pre>";
                //print_r($value2);
                //echo "</pre>";


                $idfattura = $value2['id'];
                $pdf_fattura = $value2 ['link_doc'];
                $nome_cliente = $value2 ['nome'];
                $importo_lordo = $value2 ['importo_totale'];

                $url = "https://api.fattureincloud.it:443/v1/fatture/infomail";

                include plugin_dir_path(__FILE__) . 'conn_curl_email.php';



                // print_r($result);

                
                //print_r($result['mail_destinatario']);

                // print $pdf_fattura;

                if (!empty($result['mail_destinatario']) || !empty($result['mail_cc'])) {

                    echo "<form id=\"send_email_fattureincloud$idfattura\" method=\"POST\">";

                    $email_destinatario = $result['mail_destinatario'];

                    if (empty($result['mail_destinatario'])) {

                             $email_destinatario = $result['mail_cc'];

                    }


                    print "<a href=\"https://secure.fattureincloud.it/invoices-view-".$idfattura."\">Visualizza fattura su Fattureincloud</a><br>";
                    print "<a href=\"$pdf_fattura\">Visualizza PDF fattura</a><br>";


                    print "<b>".$result['oggetto_default']."</b><br>";
                    print " Destinatario: ".$nome_cliente."<br>";
                    print " <b>importo iva inclusa</b> €".$importo_lordo." <br> ";
                    print "<b>email</b> ".$email_destinatario."<br>";



                    $oggetto_email = $result['oggetto_default'];

                    $nome_cliente_fic = $nome_cliente;

                    $oggetto_ordine_fic = $oggetto_email;

                    echo "<input type=\"hidden\" value=\"$email_destinatario\" name=\"email_destinatario\" />";

                    echo "<button type=\"submit\" name=\"$idfattura\" value=\"$idfattura\" class=\"button button-primary\" >Invia Fattura via Email</button><hr>";

                    if (isset($_POST[$idfattura])) {

                        include plugin_dir_path(__FILE__) . '/send_email_fattureincloud.php';

                    }

                    echo "</form><br>";
                }


                if ($count == 5) {

                    print "numero massimo ( 5 ) di fatture visualizzabili raggiunto";
                    break;

                } 

            }

        }

    }


} echo "</div>";
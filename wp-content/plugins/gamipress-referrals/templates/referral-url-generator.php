<?php
/**
 * Referral URL Generator template
 *
 * This template can be overridden by copying it to yourtheme/gamipress/referrals/referral-url-generator.php
 */
global $gamipress_referrals_template_args;
$user = wp_get_current_user();
// Shorthand

$whatsapp_message = "Hey ciao, ho trovato questa community di WeShoot che ti volevo far conoscere! Ti aspetto su https://www.weshoot.it/accademia/?ref=" . $user->user_login;
$whatsapp_message = urlencode($whatsapp_message);

$a = $gamipress_referrals_template_args; ?>

<div class="gamipress-referrals-form gamipress-referrals-referral-url-generator">


    <p class="gamipress-referrals-form-url">

        <label for="gamipress-referrals-form-url-input">Condividi questo URL con i tuoi amici su Instagram Stories,
            Whatsapp o Facebook</label>

        <input type="text" id="gamipress-referrals-form-url-input" class="gamipress-referrals-form-url-input"
               value="https://www.weshoot.it/accademia/?ref=<?php echo($user->user_login) ?>">

    </p>


    <div class="gamipress-social-share gamipress-social-share-alignment-left"
         data-url="https://www.weshoot.it/accademia/?ref=<?php echo($user->user_login) ?>&utm_campaign=friend_referral&utm_source=referral">


        <div class="gamipress-social-share-buttons">
            <div class="gamipress-social-share-button gamipress-social-share-facebook">

                <div class="gamipress-social-share-facebook-share"
                     data-href="https://www.weshoot.it/accademia/?ref=<?php echo($user->user_login) ?>&utm_campaign=friend_referral&utm_source=referral"
                     data-layout="button" data-size="large">
                    <span>Condividi</span>
                </div>

            </div>

            <div class="gamipress-social-share-button gamipress-social-share-whatsapp">
                <a class="gamipress-social-share-whatsapp-share"
                   href="https://wa.me/?text=<?php echo $whatsapp_message; ?>">
                    Invita su Whatsapp
                </a>
            </div>
        </div>

    </div>


</div>

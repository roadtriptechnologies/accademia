��          �       <      <     =     D     Z     l     �     �     �     �     �     �     �     �          &     7     ?     F  �  S     M     U     j  &   {     �     �     �     �     �     �  *   �  "        ?     _  	   q     {  	   �   Access Access not restricted Access restricted Access to this page restricted! Achievement Type Achievement(s) Achievement: All Content Unlock Get access to %s Get access to %s using points Get access using %d %s Get access using %s Insufficient %s. Message Points Requirements Project-Id-Version: GamiPress Restrict Content
Report-Msgid-Bugs-To: https://gamipress.com/
POT-Creation-Date: 2020-04-14 11:57+0200
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2020-10-22 16:25+0000
Last-Translator: Marco Carotenuto
Language-Team: Italiano
X-Generator: Loco https://localise.biz/
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c
Language: it_IT
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: libraries
X-Loco-Version: 2.4.3; wp-5.5.1 Accesso Accesso non limitato Accesso limitato L'accesso a questa pagina è limitato! Tipo di Sfida Sfida(s) Sfida: Tutto Contenuto Sbloccato Ottieni l'accesso a %s Ottieni l'accesso a %s utilizzando i punti Ottieni l'accesso utlizzando %d %s Ottieni l'accesso utlizzando %s %s insufficienti. Messaggio Punti Requisiti 
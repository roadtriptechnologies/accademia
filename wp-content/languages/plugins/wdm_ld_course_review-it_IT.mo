��    <      �  S   �      (     )     1  
   9  g   D     �     �     �     �     �  -   �       	   *     4     @  ,   ]     �     �     �  (   �     �     �     �     �     �  	             /     I     R     r     �     �     �     �     �     �     �                    ,     >  6   P     �     �  !   �     �     �  1   �  *    	     K	     j	     y	  c   �	     �	     �	  
   
     
     5
  �  <
               (  c   6     �     �     �     �     �  1   �          0     @  +   T  1   �     �     �     �  0   �               (     <     O  
   j     u     �     �     �     �     �     �          #     7     R     k     �     �     �     �     �  H   �  
   .     9  &   V     }     �  *   �  1   �           "     8  i   W     �     �  
   �     �  
                         <                                1   8   %                                 /      -          4              5      !   '      .   0   $   
   &                  +   *      )         #   2                       ,      (   6   9         "                3      	   7           :   ;     Review  review %s Reviews *This feedback will be privately emailed to the author, if you want to review/rate the %1$s please %2$s 1 star only 2 star only 3 star only 4 star only 5 star only <span class="reviews-total">%d</span> Ratings Add New %s Feedback All Stars All reviews Amazing, above expectations! Are you sure you want to delete your review? Avg. Rating Back Cancel Currently we are processing your review. Delete Delete Your Review? Edit Review Edit rating Edit your Review Filter by Good, what I expected Headline for your review* Helpful? How would you rate this course? Login to Review Most Recent No Review Found. No Reviews Found No Reviews Found! Please provide feedback Ratings and Reviews Review description* Save & Continue Save & Exit Send Feedback Show less replies Show more reviews Something seems to be wrong!!! Please try again later. Sort by Thank you for your feedback Thanks for submitting the review! Top Ratings View %d more replies We will make it public once it has been approved. What's your experience? We'd love to know! Why did you leave this rating? Write a Review Yes, Delete My Review You cannot rate 0 stars. Please go back to the previous step and select a minimum of 1 star rating. Your Review Your feedback on %s click here remaining character(s) review Project-Id-Version: LearnDash Ratings, Reviews, and Feedback 2.0.1
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/wdm-course-review
Last-Translator: Marco
Language-Team: Italiano
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2020-11-28 14:23+0100
X-Generator: Poedit 2.4.2
X-Domain: wdm_ld_course_review
Language: it_IT
Plural-Forms: nplurals=2; plural=n != 1;
X-Loco-Version: 2.4.6; wp-5.5.3
  Recensione  recensione %s Recensioni *Questo feedback sarà inviato privatamente all’autore, se vuoi recensire il %1$s per favore %2$s solo 1 stelle solo 2 stelle solo 3 stelle solo 4 stelle solo 5 stelle <span class="reviews-total">%d</span> Valutazioni Aggiungi un nuovo %s Feedback Tutte le Stelle Tutte le Recensioni Incredibile, al di sopra delle aspettative! Sei sicuro di voler cancellare la tua recensione? Media Valutazioni Indietro Cancella Attualmente stiamo elaborando la tua recensione. Cancella Cancella la recensione? Modifica Recensione Modifica votazione Modifica la tua Recensione Filtra per Ottimo, quello che mi aspettavo Titolo per la tua recensione* Utile? Come valuti questo corso? Fai il login per Recensire Più Recenti Nessuna Recensione. Nessuna Recensione Nessuna Recensione! Scrivi qui la tua opinione Valutazioni e Recensioni Descrizione della Recensione* Salva e Continua Salva e Esci Invia Recensione Mostra meno risposte Mostre altre recensioni Sembra che ci sia qualcosa che non va! Si prega di riprovare più tardi. Ordina per Grazie per la tua recensione Grazie per aver inviato la recensione! Top Recensori Mostra %d altre risposte La renderemo pubblica una volta approvata. Qual è la tua esperienza? Ci piacerebbe saperlo! Valuta il corso da 1 a 5 stelle? Lascia una Recensione Si, cancella la mia Recensione Non si può votare 0 stelle. Si prega di tornare al passo precedente e selezionare un minimo di 1 stella. La tua Recensione La tua opinione su %s clicca qui caratteri rimanenti recensione 
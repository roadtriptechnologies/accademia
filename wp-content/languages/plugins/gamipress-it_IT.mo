��          �       �      �     �     �     �  	   �  
   �     �  #        (     -  "   J     m     |     �     �     �     �     �     �     �             	     	   "     ,  �  F  	   ,     6     C     J     X     g  %   �     �  $   �  +   �     �          #     3     G     ]     s     �     �     �     �  
   �     �  $   �   %s Achievements %s Requirements Achievement Earned By Earned By: For more information: How this achievement can be earned. Name People who have earned this: People who have reached this rank: Points Awarded Reach a Rank Required Steps Requirement Requirements Share on Facebook Share on LinkedIn Share on Pinterest Share on Twitter Share: Show Requirements Today Unlimited Yesterday You have reached this %s! Project-Id-Version: Gamipress
Report-Msgid-Bugs-To: https://tsunoa.com/
POT-Creation-Date: 2020-10-21 16:43+0200
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2020-11-04 15:26+0000
Last-Translator: Marco Carotenuto
Language-Team: Italiano
X-Generator: Loco https://localise.biz/
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c
Language: it_IT
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: libraries
X-Loco-Version: 2.4.4; wp-5.5.3 %s Badges %s Requisiti Badges Guadagnato da Guadagnato da: Per maggiori informazioni: Come si può guadagnare questo badges Nome Persone che hanno guadagnato questo: Persone che hanno raggiunto questo livello: Punti Assegnati Raggiungere un rango Steps Richiesti Requisito Requisiti Condividi su Facebook Condividi su LinkedIn Condividi su pinterest Condividi su Twitter Condividi su: Mostra Requisiti Oggi Illimitato Ieri Complimenti hai raggiunto questo %s! 
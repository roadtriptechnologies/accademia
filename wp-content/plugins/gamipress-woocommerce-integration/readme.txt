=== GamiPress - WooCommerce integration ===
Contributors: gamipress, tsunoa, rubengc, eneribs
Tags: ecommerce, store, gamipress, gamification, points, achievements, badges, awards, rewards, credits, engagement, woocommerce, wc, product
Requires at least: 4.4
Tested up to: 5.7
Stable tag: 1.3.5
License: GNU AGPLv3
License URI: http://www.gnu.org/licenses/agpl-3.0.html

Connect GamiPress with WooCommerce

== Description ==

Gamify your [WooCommerce](http://wordpress.org/plugins/woocommerce/ "WooCommerce") store thanks to the powerful gamification plugin, [GamiPress](https://wordpress.org/plugins/gamipress/ "GamiPress")!

This plugin automatically connects GamiPress with WooCommerce adding new activity events and features.

= New Events =

* New product: When a user publish a new product.
* New purchase: When a user makes a new purchase (triggered 1 time on a purchase, independent of the number of products purchased).
* New product purchase: When a user purchases a product (triggered on every product purchased on a purchase).
* Specific product purchase: When a user purchases a specific product.
* Specific product variation purchase: When a user purchases a specific product variation.
* New product purchase of a specific category: When a user purchases a product of a specific category.
* New product purchase of a specific tag: When a user purchases a product of a specific tag.
* Review a product: When a user reviews a product.
* Review a specific product: When a user reviews a specific product.
* Vendor gets a review on a product: When a user (vendor) gets a review on a product.
* Vendor gets a review on a specific product: When a user (vendor) gets a review on a specific product.
* New sale: When a user (vendor) makes a new sale.
* Refund a purchase: When a user refunds a purchase.
* Refund a product: When a user refunds a product (triggered on every product refunded on a purchase).
* Refund a specific product: When a user purchases a specific product.
* Refund a specific product variation: When a user refunds a specific product variation.
* Refund a product of a specific category: When a user refunds a product of a specific category.
* Refund a product of a specific tag: When a user refunds a product of a specific tag.
* Get a new refund: When a user (vendor) gets a product refunded.

= Subscriptions Events =

* Subscription product purchase: When a user purchases a subscription product.
* Specific subscription product purchase: When a user purchases a specific subscription product.
* Renew a subscription product: When a user renews a subscription product.
* Renew a specific subscription product: When a user renews a specific subscription product.
* Cancel a subscription product: When a user cancels a subscription product.
* Cancel a specific subscription product: When a user cancels a specific subscription product.
* Subscription product expired: When a user subscription product expires.
* Specific subscription product expired: When a user specific subscription product expires.

= New Features =

* From the product edit screen you will be able to setup an amount of points to award customers for purchase it.

= Expand your WooCommerce integration =

There are more add-ons that improves your experience with GamiPress and WooCommerce:

* [WooCommerce Partial Payments](https://gamipress.com/add-ons/gamipress-wc-partial-payments/)
* [WooCommerce Points Gateway](https://gamipress.com/add-ons/gamipress-wc-points-gateway/)
* [WooCommerce Discounts](https://gamipress.com/add-ons/gamipress-wc-discounts/)

== Installation ==

= From WordPress backend =

1. Navigate to Plugins -> Add new.
2. Click the button "Upload Plugin" next to "Add plugins" title.
3. Upload the downloaded zip file and activate it.

= Direct upload =

1. Upload the downloaded zip file into your `wp-content/plugins/` folder.
2. Unzip the uploaded zip file.
3. Navigate to Plugins menu on your WordPress admin area.
4. Activate this plugin.

== Frequently Asked Questions ==

== Screenshots ==

== Changelog ==

= 1.3.5 =

* **Improvements**
* Improvements recount activity tool to match with the items per loop option.

= 1.3.4 =

* **Improvements**
* Improvements on recount activity tool on large websites.

= 1.3.3 =

* **Improvements**
* Ensure to only award points one 1 time even if the payment gateway marks the order as completed multiples times.
* On revoke an order, deduct the points awarded through the product settings.

= 1.3.2 =

* **Improvements**
* Ensure that cancelled subscription event is triggered only when subscription has the status "cancelled".

= 1.3.1 =

* **Improvements**
* Add a user earning entry when awarding points through the product "Award points" settings.

= 1.3.0 =

* **Improvements**
* Updated deprecated jQuery functions.
* Moved old changelog to changelog.txt file.

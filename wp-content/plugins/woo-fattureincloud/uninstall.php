<?php

/* If uninstall not called from WordPress exit */

if (!defined('WP_UNINSTALL_PLUGIN')) {

    exit;

}

// Delete option from options table
delete_option('api_key_fattureincloud');

delete_option('api_uid_fattureincloud');

delete_option('woo_fattureincloud_order_id');

delete_option('fattureincloud_auto_save');

delete_option('woo-fattureincloud-anno-fatture');

delete_option('fattureincloud_partiva_codfisc');

delete_option('fattureincloud_paid');

delete_option('fattureincloud_send_choice');

delete_option('update_customer_registry');

delete_option('show_short_descr');

delete_option('delete_autosave_fattureincloud');


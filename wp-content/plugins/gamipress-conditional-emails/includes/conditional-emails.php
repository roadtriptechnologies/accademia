<?php
/**
 * Conditional Emails Functions
 *
 * @package     GamiPress\Conditional_Emails\Conditional_Emails_Functions
 * @since       1.0.0
 */
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) exit;

/**
 * Get the registered conditional emails statuses
 *
 * @since  1.0.0
 *
 * @return array Array of conditional emails statuses
 */
function gamipress_conditional_emails_get_conditional_email_statuses() {

    return apply_filters( 'gamipress_conditional_emails_get_conditional_email_statuses', array(
        'active'    => __( 'Active', 'gamipress-conditional-emails' ),
        'inactive'  => __( 'Inactive', 'gamipress-conditional-emails' ),
    ) );

}

/**
 * Get the registered conditional emails conditions
 *
 * @since  1.0.0
 *
 * @return array Array of conditional emails conditions
 */
function gamipress_conditional_emails_get_conditional_email_conditions() {

    return apply_filters( 'gamipress_conditional_emails_get_conditional_email_conditions', array(
        'points-balance'        => __( 'Reach a points balance', 'gamipress-conditional-emails' ),
        'specific-achievement'  => __( 'Unlock a specific achievement', 'gamipress-conditional-emails' ),
        'any-achievement'       => __( 'Unlock any achievement of type', 'gamipress-conditional-emails' ),
        'all-achievements'     	=> __( 'Unlock all achievements of type', 'gamipress-conditional-emails' ),
        'specific-rank'         => __( 'Reach a specific rank', 'gamipress-conditional-emails' ),
    ) );

}

/**
 * Get all active conditional emails
 *
 * @since  1.0.0
 *
 * @return array
 */
function gamipress_conditional_emails_all_active_conditional_emails() {

    global $wpdb;

    // Setup table
    $ct_table = ct_setup_table( 'gamipress_conditional_emails' );

    // Search all conditional emails actives and published before current date
    $conditional_emails = $wpdb->get_results( $wpdb->prepare(
        "SELECT *
        FROM {$ct_table->db->table_name} AS cn
        WHERE cn.status = %s
          AND cn.date < %s",
        'active',
        date( 'Y-m-d 00:00:00', current_time('timestamp') )
    ) );

    ct_reset_setup_table();

    /**
     * Filter all active conditional emails
     *
     * @since  1.0.0
     *
     * @param array     $conditional_emails
     *
     * @return array
     */
    return apply_filters( 'gamipress_conditional_emails_all_active_conditional_emails', $conditional_emails );

}

/**
 * Get all active conditional emails based on given condition
 *
 * @since  1.0.0
 *
 * @param string $condition
 *
 * @return array
 */
function gamipress_conditional_emails_get_active_conditional_emails( $condition ) {

    global $wpdb;

    $prefix = '_gamipress_conditional_emails_';

    // Setup table
    $ct_table = ct_setup_table( 'gamipress_conditional_emails' );

    // Search all conditional emails actives, published before current date and based on a given condition
    $conditional_emails = $wpdb->get_results( $wpdb->prepare(
        "SELECT *
        FROM {$ct_table->db->table_name} AS cn
        LEFT JOIN {$ct_table->meta->db->table_name} AS cnm ON ( cn.conditional_email_id = cnm.conditional_email_id AND cnm.meta_key = %s )
        WHERE cnm.meta_value = %s
          AND cn.status = %s
          AND cn.date < %s",
        $prefix . 'condition',
        $condition,
        'active',
        date( 'Y-m-d 00:00:00', current_time('timestamp') )
    ) );

    ct_reset_setup_table();

    /**
     * Filter active conditional emails based on given condition
     *
     * @since  1.0.0
     *
     * @param array     $conditional_emails
     * @param string    $condition
     *
     * @return array
     */
    return apply_filters( 'gamipress_conditional_emails_get_active_conditional_emails', $conditional_emails, $condition );

}
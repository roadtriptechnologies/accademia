<?php

// Don't access this directly, please
if (!defined('ABSPATH')) exit;


$fattureincloud_url = "https://api.fattureincloud.it:443/v1/fatture/nuovo";

$api_uid = get_option('api_uid_fattureincloud');
$api_key = get_option('api_key_fattureincloud');


$lista_articoli = array();


//$aggiungi_shipping = array();

//$spedizione_netta = $order_data['shipping_total'] - ($order_data['shipping_total'] - round(($order_data['shipping_total'] / 122) * 100, 2));

//$spedizione_lorda = $order_data['shipping_total'] + $order_data['shipping_tax'] ;

$spedizione_lorda = $order_data['shipping_total'] + $order_shipping_tax ;


//$spedizione_netta = $spedizione_lorda - $order_data['shipping_tax'];

$spedizione_netta = $spedizione_lorda - $order_shipping_tax;



$codice_iva = '';


foreach ($order->get_items() as $item_key => $item_values):

    $item_data = $item_values->get_data();


    $line_total = $item_data['total'];


    $product_id = $item_values->get_product_id(); // the Product id
    $wc_product = $item_values->get_product(); // the WC_Product object
    /* Access Order Items data properties (in an array of values) */
    $item_data = $item_values->get_data();
    $_product = wc_get_product($product_id);

    //$tax_rates = WC_Tax::get_rates($_product->get_tax_class());
    $tax_rates = WC_Tax::get_base_tax_rates($_product->get_tax_class(true));

   
   
    if (!empty($tax_rates)) {
   
        $tax_rate = reset($tax_rates);

        
        if ($tax_rate['rate'] == 22) {

            $codice_iva = 0;


        } elseif ($tax_rate['rate'] == 0) {

            if ($item_data['tax_class'] === 'zero-rate-n1') {

                $codice_iva = 21;

            } elseif ($item_data['tax_class'] === 'zero-rate-n2') {

                $codice_iva = 10;

            } elseif ($item_data['tax_class'] === 'zero-rate-n3') {

                $codice_iva = 12;

            } elseif ($item_data['tax_class'] === 'zero-rate-n4') {

                $codice_iva = 46;

            } elseif ($item_data['tax_class'] === 'zero-rate-n5') {

                $codice_iva = 30;

            } elseif ($item_data['tax_class'] === 'zero-rate-n6') {

                $codice_iva = 11;

            } elseif ($item_data['tax_class'] === 'zero-rate-n7') {

                $codice_iva = 16;

            } else { 
                
                $codice_iva = 6;  
            
            }


        } elseif ($tax_rate['rate'] == 4) {

            $codice_iva = '';

        } elseif ($tax_rate['rate'] == 10) {

            $codice_iva = '';

        }

    } elseif (empty($tax_rates)) {

        $codice_iva = 6;
    }


       // $prezzo_singolo_prodotto = (round($item_data['total'], 2) / $item_data['quantity']);

    $prezzo_singolo_prodotto = ((round($item_data['total'], 2)+$item_data['total_tax']) / $item_data['quantity']);
    $ivatosiono = true;


    $mostra_brevedesc = '';

    if (1 == get_option('show_short_descr')) {

        $mostra_brevedesc = $wc_product->get_short_description();

    }


    $lista_articoli[] = array(
        "nome" => $item_data['name'],
        "codice" => $wc_product->get_sku(),
        "descrizione" => $mostra_brevedesc,
        "quantita" => $item_data['quantity'],
        "cod_iva" => $codice_iva,
        "prezzo_netto" => $prezzo_singolo_prodotto,
        "prezzo_lordo" => $prezzo_singolo_prodotto

    );

    if ('paypal' == $order_billing_payment_method) {

        $payment_method_fic = 'MP08';

    } elseif ('stripe' == $order_billing_payment_method) {

        $payment_method_fic = 'MP08';

    } elseif ('bacs' == $order_billing_payment_method) {

        $payment_method_fic = 'MP05';

    } elseif ('cheque' == $order_billing_payment_method) {

        $payment_method_fic = 'MP02';

    } elseif ('cod' == $order_billing_payment_method) {

        $payment_method_fic = 'MP01';

    } else {

        $payment_method_fic = 'MP01';

    }


endforeach;

if ($order_data['shipping_total'] > 0) {

    if ($order_data['shipping_tax'] == 0) {

        $cod_shipping_iva = 6;
        
    } else {

        $cod_shipping_iva = 0;

    }

    $lista_articoli[] 
        
        =   array(

            "nome" => "Spese di Spedizione",
            "quantita" => 1,
            "cod_iva" => $cod_shipping_iva,
            "prezzo_netto" => $spedizione_netta,
            "prezzo_lordo" => $spedizione_lorda


        );

}



//print_r($lista_articoli);

if (isset($_POST['woo-datepicker'])) { 

        $data_di_scadenza;
        $data_di_scadenza = $_POST['woo-datepicker'];
        //echo $_POST['woo-datepicker'];
        
} else {

    $data_di_scadenza = date('d/m/Y');

}


$salva_ononsalva = false;

if (1 == get_option('update_customer_registry')) {

    $salva_ononsalva = true;

}

if (1 == get_option('fattureincloud_paid')) {

    $fattureincloud_invoice_paid = $order_data['payment_method'];
    $mostra_info_pagamento = true;
    $data_saldo = $order_data['date_created']->date('d/m/Y');
    $data_scadenza = date('d/m/Y');

} elseif (0 == get_option('fattureincloud_paid')) {

    $fattureincloud_invoice_paid = 'not';
    $mostra_info_pagamento = false;
    $data_saldo = 'not';
    $data_scadenza = $data_di_scadenza; //date('d/m/Y');

}




$fattureincloud_request = array(

    "api_uid" => $api_uid,
    "api_key" => $api_key,
    "nome" => $order_billing_first_name . " " . $order_billing_last_name . " " . $order_billing_company,
    "indirizzo_via" => $order_billing_address_1,
    "indirizzo_cap" => $order_billing_postcode,
    "indirizzo_citta" => $order_billing_city,
    "indirizzo_provincia" => $order_billing_state,
    "paese_iso" => $order_billing_country,
    "prezzi_ivati" => $ivatosiono ,
    /*"indirizzo_extra" => $order_billing_email,*/
    "piva" => $order_billing_partiva,
    "cf" => $order_billing_codfis,
    "salva_anagrafica" => $salva_ononsalva,
    "oggetto_visibile" => "Ordine numero ".$id_ordine_scelto,
    "note" => $order_note,
    "mostra_info_pagamento" => $mostra_info_pagamento,
    "metodo_pagamento" => "Metodo ". $order_billing_method,
    "lista_articoli" => $lista_articoli,
    "lista_pagamenti" => array(
        array(
            "data_scadenza" => $data_scadenza,
            "importo" => 'auto',
            "metodo" => $fattureincloud_invoice_paid,
            "data_saldo" => $data_saldo ,

        )
    ),
    "PA" => $fatturaelettronica_fic,
    "PA_tipo_cliente" => 'B2B',
    "PA_data" => $data_scadenza,
    "PA_pec" => $order_billing_emailpec,
    "PA_codice" => $order_billing_coddest,
    "PA_modalita_pagamento" => $payment_method_fic,
    "extra_anagrafica" => array(
        "mail" => $order_billing_email,
        "tel" => $order_billing_phone
        )
);


$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $fattureincloud_url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fattureincloud_request));

$headers = array();
$headers[] = 'Content-Type: application/json';
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$pre_result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close($ch);

$fattureincloud_result = json_decode($pre_result, true);

//print_r($fattureincloud_result);

/*

$fattureincloud_options = array(
    "http" => array(
        "header" => "Content-type: text/json\r\n",
        "method" => "POST",
        "content" => json_encode($fattureincloud_request)
    ),
);
$fattureincloud_context = stream_context_create($fattureincloud_options);
$fattureincloud_result = json_decode(file_get_contents($fattureincloud_url, false, $fattureincloud_context), true);

*/
//print_r($fattureincloud_result);
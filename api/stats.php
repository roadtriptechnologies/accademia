<?php

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once("$root/accademia/api/conf.php");
require_once("$root/accademia/api/customerly.php");
require_once("$root/accademia/api/webhook.php");

use Slack\Message\{Attachment, AttachmentField};

class Stats
{


    static function getInsigths($days)
    {
        global $mysqli;
        $insights = array();

        $query = "SELECT count(*) AS signups, DATE_FORMAT(user_registered, '%a %d %M') as day,
(SELECT SUM(net_total) FROM wp_wc_order_stats AS S  WHERE DATE(S.date_created) = DATE(user_registered)) AS spent,
(SELECT count(*) FROM wp_learndash_user_activity WHERE DATE(FROM_UNIXTIME(activity_started)) = DATE(user_registered)  AND activity_type = 'course'  ) as started_courses,
(SELECT count(*) FROM wp_learndash_user_activity WHERE DATE(FROM_UNIXTIME(activity_started)) = DATE(user_registered)  AND activity_type = 'lesson' ) AS started_lessons,
(SELECT count(*) FROM wp_learndash_user_activity WHERE DATE(FROM_UNIXTIME(activity_completed)) = DATE(user_registered)  AND activity_type = 'course'  ) as completed_courses,
(SELECT count(*) FROM wp_learndash_user_activity WHERE DATE(FROM_UNIXTIME(activity_completed)) = DATE(user_registered)  AND activity_type = 'lesson' ) AS completed_lessons,
(SELECT COUNT(net_total) FROM wp_wc_order_stats AS S  WHERE DATE(S.date_created) = DATE(user_registered)) AS orders
FROM wp_users GROUP BY DATE(user_registered) ORDER BY user_registered DESC LIMIT ?";

        if ($stmt = $mysqli->prepare($query)) {
            $stmt->bind_param('s', $days);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($users, $day, $revenue, $started_courses, $started_lessons, $completed_courses, $completed_lessons, $orders);

            while ($stmt->fetch()) {
                $insights[] = array(
                    'day' => $day,
                    'users' => $users,
                    'revenue' => $revenue,
                    'started_courses' => $started_courses,
                    'started_lessons' => $started_lessons,
                    'completed_courses' => $completed_courses,
                    'completed_lessons' => $completed_lessons,
                    'orders' => $orders

                );
            }

            return $insights;

        }
    }

    static function getTodaySignups()
    {


        $newLine = "\n";
        $tab = "\t\t";
        $message = "📈 Riepilogo Giornaliero" . $newLine . $newLine;

        $days = Stats::getInsigths(1);

        foreach ($days as $d) {
            $message .= "📆️ " . $d['day'] . $newLine . $newLine .
                $tab . "👨‍🎓 +" . number_format($d['users'], 0, ',', '.') . " WeShooters" . $newLine
                . $tab . "📖 " . $d['started_lessons'] . " Lezioni iniziate, " . $d['completed_lessons'] . " completate " . $newLine
                . $tab . "📚 " . $d['started_courses'] . " Corsi iniziati, " . $d['completed_courses'] . " completati " . $newLine
                . $tab . ($d['revenue'] > 0 ? "💰 " . $d['revenue'] . $newLine : "")
                . $tab . ($d['revenue'] > 0 ? "🛒 " . $d['orders'] : "")
                . $newLine;

        }


        Slack::send("marketing", $message);
    }

    static function getWeeklySignups()
    {


        $newLine = "\n";
        $tab = "\t\t";
        $message = "📈 Riepilogo Settimanale" . $newLine . $newLine;

        $total_users = 0;
        $total_revenue = 0;
        $total_orders = 0;

        $days = Stats::getInsigths(7);

        foreach ($days as $d) {
            $message .= "📆️ " . $d['day'] . $newLine . $newLine .
                $tab . "👨‍🎓 +" . number_format($d['users'], 0, ',', '.') . " WeShooters" . $newLine
                . $tab . "📖 " . $d['started_lessons'] . " Lezioni iniziate, " . $d['completed_lessons'] . " completate " . $newLine
                . $tab . "📚 " . $d['started_courses'] . " Corsi iniziati, " . $d['completed_courses'] . " completati " . $newLine
                . $tab . ($d['revenue'] > 0 ? "💰 " . $d['revenue'] . $newLine : "")
                . $tab . ($d['revenue'] > 0 ? "🛒 " . $d['orders'] : "")
                . $newLine;

            $total_users += $d['users'];
            $total_revenue += $d['revenue'];
            $total_orders += $d['orders'];
        }


        $message .= $newLine . "🌅 Totali Settimana " . $newLine . $newLine
            . "👨‍🎓 WeShooters +" . number_format($total_users, 0, ',', '.') . $newLine
            . "💰 Revenue " . ($total_revenue > 0 ? "€$total_revenue " : "") . $newLine
            . "🛒 Orders " . ($total_orders > 0 ? " $total_orders " : "") . $newLine
            . $newLine;


        Slack::send("marketing", $message);
    }

    static function getUserInfo()
    {
        global $mysqli;

        $total_users = 0;
        $total_coins = 0;
        $total_courses_started = 0;
        $total_courses_completed = 0;
        $total_orders = 0;
        $total_revenue = 0;

        $query = "SELECT user_email, display_name,
            (SELECT meta_value FROM wp_usermeta WHERE meta_key = '_gamipress_coins_points' AND user_id = U.ID) AS coins,
            (SELECT meta_value FROM wp_usermeta WHERE meta_key = '_gamipress_pixels_points' AND user_id = U.ID) AS pixels,
            (SELECT meta_value FROM wp_usermeta WHERE meta_key = '_gamipress_livello_rank' AND user_id = U.ID) AS rank,
            (SELECT SUM(net_total) FROM wp_wc_customer_lookup AS C
            JOIN wp_wc_order_stats AS S ON S.customer_id = C.customer_id WHERE C.user_id = U.ID) AS spent,
            (SELECT COUNT(net_total) FROM wp_wc_customer_lookup AS C
            JOIN wp_wc_order_stats AS S ON S.customer_id = C.customer_id WHERE C.user_id = U.ID) AS orders,
            (SELECT count(*) FROM wp_learndash_user_activity WHERE user_id = U.ID AND activity_type = 'course'  ) as started_courses,
            (SELECT count(*) FROM wp_learndash_user_activity WHERE user_id = U.ID AND activity_type = 'course' AND activity_completed  > 0 ) as completed_courses,
            (SELECT count(*) FROM wp_learndash_user_activity WHERE user_id = U.ID AND activity_type = 'lesson' ) AS started_lessons,
            (SELECT count(*) FROM wp_learndash_user_activity WHERE user_id = U.ID AND activity_type = 'lesson' AND activity_completed > 0) AS completed_lessons
            FROM wp_users AS U";


        $users = array();


        if ($stmt = $mysqli->prepare($query)) {
            //$stmt->bind_param('ss', $uid, $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($email, $name, $coins, $pixels, $rank, $spent, $orders, $started_courses, $completed_courses, $started_lessons, $completed_lessons);
            while ($stmt->fetch()) {
                $users[] = array(
                    'email' => $email,
                    'name' => $name,
                    'attributes' => array(
                        'orders' => $orders,
                        'spent' => $spent,
                        'coins' => $coins,
                        'pixels' => $pixels,
                        'rank' => $rank,
                        'started_courses' => $started_courses,
                        'completed_courses' => $completed_courses,
                        'started_lessons' => $started_lessons,
                        'completed_lessons' => $completed_lessons,
                    )
                );
                $total_users++;
                $total_coins += $coins;
                $total_courses_started += $started_courses;
                $total_courses_completed += $completed_courses;
                $total_orders += $orders;
                $total_revenue += $spent;
            }
        }


        $newLine = "\n";
        $message = "📈 Statistiche ad oggi" . $newLine . $newLine;

        $message .= "➡️ Corsi iniziati " . number_format($total_courses_started, 0, ',', '.') . $newLine;
        $message .= "✅️ Corsi terminati " . number_format($total_courses_completed, 0, ',', '.') . $newLine;

        $message .= "📷️ WeShooters totali " . number_format($total_users, 0, ',', '.') . $newLine;
        $message .= "🎰 Coins in circolazione: " . number_format($total_coins, 0, ',', '.') . $newLine;
        $message .= "🎯 Ordini Totali " . number_format($total_orders, 0, ',', '.') . $newLine;
        $message .= "💰 Totale Revenue Acquisti €" . number_format($total_revenue, 2, ',', '.') . $newLine . $newLine;


        Slack::send("marketing", $message);


    }

    static function syncUserInfo()
    {
        global $mysqli;

        $total_users = 0;
        $total_coins = 0;
        $total_courses_started = 0;
        $total_courses_completed = 0;
        $total_orders = 0;
        $total_revenue = 0;

        $query = "SELECT user_email, display_name,
            (SELECT count(*) FROM wp_usermeta WHERE meta_key = 'wp_capabilities' AND user_id = U.ID AND meta_value LIKE '%accademici%') AS master,
            (SELECT meta_value FROM wp_usermeta WHERE meta_key = '_gamipress_coins_points' AND user_id = U.ID) AS coins,
            (SELECT meta_value FROM wp_usermeta WHERE meta_key = '_gamipress_pixels_points' AND user_id = U.ID) AS pixels,
            (SELECT meta_value FROM wp_usermeta WHERE meta_key = '_gamipress_livello_rank' AND user_id = U.ID) AS rank,
            (SELECT meta_value FROM wp_usermeta WHERE meta_key = 'suspiciousReferrals' AND user_id = U.ID) AS suspiciousReferrals,
            (SELECT value FROM wp_bp_xprofile_data WHERE field_id = 4 AND user_id = U.ID) AS compleanno,
            (SELECT value FROM wp_bp_xprofile_data WHERE field_id = 5 AND user_id = U.ID) AS sesso,
            (SELECT value FROM wp_bp_xprofile_data WHERE field_id = 24 AND user_id = U.ID) AS website,
            (SELECT value FROM wp_bp_xprofile_data WHERE field_id = 25 AND user_id = U.ID) AS professione,
            (SELECT value FROM wp_bp_xprofile_data WHERE field_id = 26 AND user_id = U.ID) AS reflex,
            (SELECT value FROM wp_bp_xprofile_data WHERE field_id = 27 AND user_id = U.ID) AS ottiche,
            (SELECT value FROM wp_bp_xprofile_data WHERE field_id = 28 AND user_id = U.ID) AS residenza,
            (SELECT count(*) FROM wp_lm_referrals WHERE referral_id = U.ID) as referred,
            (SELECT SUM(net_total) FROM wp_wc_customer_lookup AS C
            JOIN wp_wc_order_stats AS S ON S.customer_id = C.customer_id WHERE C.user_id = U.ID) AS spent,
            (SELECT COUNT(net_total) FROM wp_wc_customer_lookup AS C
            JOIN wp_wc_order_stats AS S ON S.customer_id = C.customer_id WHERE C.user_id = U.ID) AS orders,
            (SELECT count(*) FROM wp_learndash_user_activity WHERE user_id = U.ID AND activity_type = 'course'  ) as started_courses,
            (SELECT count(*) FROM wp_learndash_user_activity WHERE user_id = U.ID AND activity_type = 'course' AND activity_completed  > 0 ) as completed_courses,
            (SELECT count(*) FROM wp_learndash_user_activity WHERE user_id = U.ID AND activity_type = 'lesson' ) AS started_lessons,
            (SELECT count(*) FROM wp_learndash_user_activity WHERE user_id = U.ID AND activity_type = 'lesson' AND activity_completed > 0) AS completed_lessons
            FROM wp_users AS U";


        $users = array();


        if ($stmt = $mysqli->prepare($query)) {
            //$stmt->bind_param('ss', $uid, $id);
            $stmt->execute();
            $stmt->store_result();
            $stmt->bind_result($email, $name, $master, $coins, $pixels, $rank, $suspiciousReferrals, $bday, $gender, $website, $role, $reflex, $lenses, $county, $referred, $spent, $orders, $started_courses, $completed_courses, $started_lessons, $completed_lessons);
            while ($stmt->fetch()) {

                $tags = $suspiciousReferrals > 0 ? array("furbetto") : array();
                $users[] = array(
                    'email' => $email,
                    'name' => $name,
                    'attributes' => array(
                        'master' => $master,
                        'bday' => $bday,
                        'gender' => $gender,
                        'website' => $website,
                        'role' => $role,
                        'reflex' => $reflex,
                        'lenses' => $lenses,
                        'county' => $county,
                        'orders' => $orders,
                        'spent' => $spent,
                        'coins' => $coins,
                        'pixels' => $pixels,
                        'rank' => $rank,
                        'started_courses' => $started_courses,
                        'completed_courses' => $completed_courses,
                        'started_lessons' => $started_lessons,
                        'completed_lessons' => $completed_lessons,
                        'suspicious_referrals' => $suspiciousReferrals,
                        'total_referred' => $referred
                    ),
                    "tags" => $tags
                );
                $total_users++;
                $total_coins += $coins;
                $total_courses_started += $started_courses;
                $total_courses_completed += $completed_courses;
                $total_orders += $orders;
                $total_revenue += $spent;
            }
        }

        $chunks = array_chunk($users, 99);

        foreach ($chunks as $chunk) {
            if (count($chunk) > 0) {
                Customerly::createUsers($chunk);
            }
        }


    }
}


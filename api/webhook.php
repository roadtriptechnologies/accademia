<?php


$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require_once("$root/accademia/vendor/autoload.php");

use Slack\Message\{Attachment, AttachmentField};


/**
 * SLack Messages
 */
class Slack
{

    //G014JJ222EQ -> Webhook
    //B013C9Q0PRV -> revenue

    static function send($channel, $message)
    {

        $channels = array(
            'webhooks' => 'B013C9Q0PRV/mwHR9JfEDxEpnNX7dfyWgqCl',
            'marketing' => 'B01GEGC8SMR/r0mdaGGWlbACgivQjv3sD89e',
            'controllo' => 'B013C9Q0PRV/fWfjX5ym71dnTS9yCvLfSQAA'
        );

        // Instantiate with defaults, so all messages created
        // will be sent from 'Cyril' and to the #accounting channel
        // by default. Any names like @regan or #channel will also be linked.
        $settings = [
            'username' => 'Paperone',
            'channel' => "#$channel",
            'link_names' => true,
            'icon' => '💰'
        ];
        $endpoint = "https://hooks.slack.com/services/T011DGXVB8D/" . $channels[$channel];

        $client = new Maknz\Slack\Client($endpoint, $settings);

        $client->send($message);


    }

}
